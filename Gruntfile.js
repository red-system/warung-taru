module.exports = function (grunt) {

    var jsFiles = [
        'assets/template_front/js/koad-plugins.min.js',
        'assets/template_front/js/extensions/revolution.extension.slideanims.min.js',
        'assets/template_front/js/extensions/revolution.extension.layeranimation.min.js',
        'assets/template_front/js/extensions/revolution.extension.navigation.min.js',
        'assets/template_front/js/extensions/revolution.extension.parallax.min.js',
        'assets/template_front/js/popper.js',
        'assets/template_front/js/bootstrap.min.js',
        'assets/template_front/js/swal.min.js',
        // 'assets/template_front/js/masonry.min.js',
        'assets/template_front/js/script.js',
        'assets/template_front/js/rev-slider.js',
        'assets/template_front/js/custom.js',
    ];
    var cssFiles = [
        'assets/template_front/css/google-fonts.css',
        'assets/template_front/css/koad-assets.min.css',
        'assets/template_front/css/swal.min.css',
        'assets/template_front/css/style.css',
        'assets/template_front/css/loading.css',
        'assets/template_front/css/custom.css',
    ];
    var allFiles = jsFiles.concat(cssFiles); // merge js & css files directory

    grunt.initConfig({
        jsDistDir: 'assets/template_front/js/',
        cssDistDir: 'assets/template_front/css/',
        pkg: grunt.file.readJSON('package.json'),
        concat: {
            js: {
                options: {
                    separator: ';'
                },
                src: jsFiles,
                dest: '<%=jsDistDir%>scripts-bundle.js'
            },
            css: {
                src: cssFiles,
                dest: '<%=cssDistDir%>styles-bundle.css'
            }
        },
        remove_comments: {
            js: {
              options: {
                multiline: true,
                singleline: false,
                keepSpecialComments: false
              },
              cwd: 'assets/template_front/js/',
              src: 'scripts-bundle.js',
              expand: true,
              dest: 'assets/template_front/js/',
            },
            css: {
                options: {
                  multiline: true,
                  singleline: true,
                  keepSpecialComments: true,
                  linein: true,
                  isCssLinein: true
                },
                // src: cssFiles,
                cwd: 'assets/template_front/css/',
                src: 'styles-bundle.css',
                expand: true,
                dest: 'assets/template_front/css/'
            }
        },
        uglify: {
            // mangle: {
            //     reserved: ['jQuery']
            // },
            options: {
                banner: '/*! <%= pkg.name %> <%=grunt.template.today("dd-mm-yyyy") %> */\n'
            },
            dist: {
                files: {
                    '<%=jsDistDir%>scripts-bundle.min.js': ['<%= concat.js.dest %>']
                }
            }
        },
        cssmin: {
            add_banner: {
                options: {
                    // banner: '/*! <%= pkg.name %> <%=grunt.template.today("dd-mm-yyyy") %> */\n'
                },
                files: {
                    '<%=cssDistDir%>styles-bundle.min.css': ['<%= concat.css.dest %>']
                }
            }
        },
        watch: {
            files: allFiles,
            tasks: ['concat', 'uglify', 'cssmin']
        }
    });

    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-remove-comments');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-contrib-watch');

    grunt.registerTask('default', [
        'concat',
        // 'remove_comments',
        'uglify',
        'cssmin',
        'watch'
    ]);

};