<main class="bg_gray">
    <div class="container margin_30">
        <div class="page_header">
            <div class="breadcrumbs">
                <ul>
                    <li><a href="<?php echo site_url() ?>">Beranda</a></li>
                    <?php if($page_type == 'category') { ?>
                        <li><a href="<?php echo $this->main->permalink(array('blog')) ?>">Blog</a></li>
                    <?php } ?>
                    <li><?php echo $page->title ?></li>
                </ul>
            </div>
            <h1><?php echo $page->title ?></h1>
            <?php echo $page->description ?>
        </div>

        <div class="row">
            <div class="col-lg-9">
                <div class="widget search_blog d-block d-sm-block d-md-block d-lg-none">
                    <div class="form-group">
                        <input type="text" name="search" id="search" class="form-control" placeholder="Search..">
                        <button type="submit"><i class="ti-search"></i><span class="sr-only">Search</span></button>
                    </div>
                </div>

                <div class="row">

                    <?php foreach($blog_list as $row) {
                        $link = $this->main->permalink(array('blog', $row->category_title, $row->title)); ?>

                        <div class="col-md-6">
                            <article class="blog">
                                <figure>
                                    <a href="<?php echo $link ?>">
                                        <img src="<?php echo $this->main->image_preview_url($row->thumbnail) ?>" alt="">
                                        <div class="preview"><span>Baca Selanjutnya</span></div>
                                    </a>
                                </figure>
                                <div class="post_info">
                                    <small><?php echo $row->category_title ?> - <?php echo $this->main->date_view($row->created_at) ?></small>
                                    <h2><a href="<?php echo $link ?>"><?php echo $row->title ?></a></h2>
                                    <p><?php echo $this->main->short_desc($row->description); ?></p>
                                </div>
                            </article>
                        </div>

                    <?php } ?>
                </div>

                <div class="pagination__wrapper no_border add_bottom_30">
                    <?php echo $this->pagination->create_links() ?>
                </div>

            </div>

            <aside class="col-lg-3">
                <form action="?" method="get">
                    <div class="widget search_blog d-none d-sm-none d-md-none d-lg-block">
                        <div class="form-group">
                            <input type="text" name="search" id="search_blog" class="form-control" placeholder="Search.." value="<?php echo $keyword ?>">
                            <button type="submit"><i class="ti-search"></i><span class="sr-only">Search</span></button>
                        </div>
                    </div>
                </form>

                <div class="widget">
                    <div class="widget-title">
                        <h4>Blog Terbaru</h4>
                    </div>
                    <ul class="comments-list">
                        <?php foreach($latest_blog as $row) {
                            $link = $this->main->permalink(array('blog', $row->category_title, $row->title)); ?>
                            <li>
                                <div class="alignleft">
                                    <a href="<?php echo $link ?>"><img src="<?php echo $this->main->image_preview_url($row->thumbnail) ?>" alt="<?php echo $row->thumbnail_alt ?>" title="<?php echo $row->title ?>"></a>
                                </div>
                                <small><?php echo $row->category_title ?> - <?php echo $this->main->date_view($row->created_at) ?></small>
                                <h3><a href="<?php echo $link ?>" title=""><?php echo $row->title ?></a></h3>
                            </li>
                        <?php } ?>
                    </ul>
                </div>

                <div class="widget">
                    <div class="widget-title">
                        <h4>Kategori Blog</h4>
                    </div>
                    <ul class="cats">
                        <?php foreach($category as $row) { ?>
                            <li><a href="<?php echo $this->main->permalink(array('blog', $row->title));  ?>"><?php echo $row->title ?> <span>(<?php echo $row->total ?>)</span></a></li>
                        <?php } ?>
                    </ul>
                </div>
            </aside>
        </div>
    </div>
</main>