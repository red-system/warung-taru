$(document).ready(function () {
    var site_url = $('body').data('site-url');

    close_cart_layer_open();
    cart_add();
    cart_remove();
    cart_remove_on_page();
    cart_list_qty();
    cart_checkout();
    gallery();
    select2_run();
    address_change();
    form_send();

    wishlist_process();

    member_address_primary();
    member_address_delete();
    member_address_edit();

    midtrans_payment();

    ship_change();

});

function gallery() {
    lightbox.option({
        'resizeDuration': 200,
        'wrapAround': true
    });

    $('.gallery-photo-list').masonry({
        itemSelector: '.col-sm-6',
        columnWidth: 0
    });
}

function close_cart_layer_open() {

    $('.layer').click(function () {

        $('.top_panel').removeClass('show');
        $('.layer').removeClass('layer-is-visible');
    });
}

function cart_add() {
    $('.btn_add_to_cart a').on('click', function () {

        var id = $(this).data('id');
        var title = $(this).data('title');
        var category_title = $(this).data('category-title');
        var image = $(this).data('image');
        var price = $(this).data('price');
        var price_label = $(this).data('price-label');
        var price_old = $(this).data('price-old');
        var price_old_label = $(this).data('price-old-label');
        var product_qty = $('.prod_info [name="product_qty"]').val();
        var also_bought = $('.also-bought-list').html();

        if (typeof product_qty == 'undefined') {
            product_qty = 1;
        }

        // console.log(also_bought);

        $('.cart-add-count').html(product_qty);
        $('.cart-add-item-thumbnail').html('<img src="' + image + '" class="lazy">');
        $('.cart-add-item-title').html('<h4>' + title + '</h4>');
        $('.cart-add-item-price').html(price_label);
        if (typeof price_old_label !== undefined) {
            $('.cart-add-item-price-old').html(price_old_label);
        }

        if (typeof also_bought != 'undefined') {
            $('.cart-add-item-bought-title').html('<h4>Member lain membeli produk <u><strong>' + title + '</strong></u> juga membeli produk dibawah:</h4>');
            $('.cart-add-item-bought-list').html(also_bought);
        }

        $('.top_panel').addClass('show');
        $('.layer').addClass('layer-is-visible');

        cart_add_network(id, title, price, product_qty, image, category_title);

    });
}

function cart_add_network(id, title, price, qty, thumbnail, category_title) {
    var site_url = $('body').data('site-url');
    var route_cart_add = $('body').data('route-cart-add');
    var data = {
        id: id,
        title: title,
        price: price,
        qty: qty,
        thumbnail: thumbnail,
        category_title: category_title
    };

    loading_start();

    $.ajax({
        url: route_cart_add,
        type: 'POST',
        data: data,
        success: function (json) {
            var data = JSON.parse(json);

            $('.cart-list-review').html(data.cart_list_review);
            cart_remove();
            cart_list_review_dropdown_run();
            cart_checkout();
            loading_finish();

        }
    });
}

function cart_list_review_dropdown_run() {
    $('.dropdown-cart, .dropdown-access').hover(function () {
        $(this).find('.dropdown-menu').stop(true, true).delay(50).fadeIn(300);
    }, function () {
        $(this).find('.dropdown-menu').stop(true, true).delay(50).fadeOut(300);
    });
}

function cart_remove() {
    $('.cart-item-remove').click(function () {
        var rowid = $(this).data('rowid');
        var id_product = $(this).data('id');
        var route_cart_remove = $('body').data('route-cart-remove');
        var data = {
            rowid: rowid,
            id: id_product
        };

        Swal.fire({
            title: 'Perhatian',
            text: "Anda yakin hapus item ini ?",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Ya',
            cancelButtonText: 'Batal'
        }).then((result) => {
            $('.dropdown-cart').find('.dropdown-menu').stop(true, true).delay(50).fadeIn(300);
            if (result.value) {
                $(this).parent('li').fadeOut().remove();
                loading_start();
                $.ajax({
                    url: route_cart_remove,
                    type: 'post',
                    data: data,
                    success: function (json) {
                        var data = JSON.parse(json);

                        $('.cart-list-review').html(data.cart_list_review);
                        cart_remove();
                        cart_list_review_dropdown_run();
                        cart_checkout();

                        loading_finish();
                    }
                });
            }
        });
    });
}

function cart_remove_on_page() {
    $('.cart-item-remove-on-page').click(function () {
        var rowid = $(this).data('rowid');
        var id = $(this).data('id');
        var route_cart_remove = $('body').data('route-cart-remove');
        var data = {
            rowid: rowid,
            id: id
        };

        Swal.fire({
            title: 'Perhatian',
            text: "Anda yakin hapus item ini ?",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Ya',
            cancelButtonText: 'Batal'
        }).then((result) => {
            if (result.value) {
                $(this).parents('tr').fadeOut().remove();

                loading_start();

                total_qty();
                total_price();

                $.ajax({
                    url: route_cart_remove,
                    type: 'post',
                    data: data,
                    success: function(json) {
                        var data = JSON.parse(json);

                        $('.cart-list-review').html(data.cart_list_review);
                        cart_remove();
                        cart_list_review_dropdown_run();
                        cart_checkout();

                        loading_finish();
                    }
                });
            }
        });


    });
}

function cart_list_qty() {
    $('.cart-list-qty-input').on('keyup change', function () {
        var qty = $(this).val();
        var price = $(this).parents('tr').data('price');
        var subtotal = parseFloat(qty) * parseFloat(price);
        var currency = $('body').data('currency');

        $(this).parents('td').siblings('td.cart-list-item-subtotal').html(currency + ' ' + format_number(subtotal));
        $(this).parents('tr').data('subtotal', subtotal);

        total_qty();
        total_price();
    });

    $('.inc.button_inc, .dec.button_inc').click(function () {
        var qty = $(this).siblings('input').val();
        var price = $(this).parents('tr').data('price');
        var subtotal = parseFloat(qty) * parseFloat(price);
        var currency = $('body').data('currency');

        $(this).parents('td').siblings('td.cart-list-item-subtotal').html(currency + ' ' + format_number(subtotal));
        $(this).parents('tr').data('subtotal', subtotal);

        total_qty();
        total_price();
    });
}

function cart_checkout() {
    $('.cart-checkout').click(function (e) {

        e.preventDefault();

        var route_cart_checkout_process = $('body').data('route-cart-checkout-process');
        var route_cart_checkout_page = $('body').data('route-cart-checkout-page');
        var route_login = $('body').data('route-login');
        var route_product = $('body').data('route-product');

        $.ajax({
            url: route_cart_checkout_process,
            success: function (json) {
                var data = JSON.parse(json);

                if (data.status) {
                    window.location.href = route_cart_checkout_page;
                } else {
                    Swal.fire({
                        icon: 'warning',
                        title: data.title,
                        text: data.message,
                        showCloseButton: true,
                        showCancelButton: true,
                        focusConfirm: true,
                        confirmButtonText: data.button_confirm,
                        cancelButtonText:
                            'Kembali',
                    }).then((result) => {
                        if (result.value) {
                            if (data.type == 'login') {
                                window.location.href = route_login;
                            } else if (data.type == 'cart_empty') {
                                window.location.href = route_product;
                            }
                        }
                    })
                }
            }
        });

        return false;
    });
}

function total_qty() {
    var total_qty = 0;
    $('.cart-list-qty-input').each(function () {
        var qty = $(this).val();

        total_qty += parseFloat(qty);
    });

    $('.cart-item-total').html(total_qty);
}

function total_price() {
    var total_subtotal = 0;
    var currency = $('body').data('currency');

    $('.cart-list tbody tr').each(function () {
        var subtotal = $(this).data('subtotal');

        total_subtotal += parseFloat(subtotal);
    });

    $('.cart-price-total').html(currency + ' ' + format_number(total_subtotal));
}

function format_number(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

function wishlist_process() {
    $('.wishlist-process').click(function () {
        var self = $(this);
        var id_product = $(this).data('id');
        var process_type = $(this).data('process-type');
        var route_wishlist_process = $('body').data('route-wishlist-process');
        var current_url_enc = $('body').data('current-url-enc');
        var route_login = $('body').data('route-login');
        var login_status = $('body').data('back-status');
        var data = {
            id_product: id_product,
            process_type: process_type
        };

        if (login_status) {
            loading_start();
            $.ajax({
                url: route_wishlist_process,
                type: 'post',
                data: data,
                success: function (data) {
                    loading_finish();

                    if (process_type == 'add') {
                        self.addClass('product-item-wishlist-show');
                        self.data('process-type', 'remove');
                        Swal.fire({
                            text: "Berhasil menambah ke wishlist",
                            icon: 'success',
                        });
                    } else if (process_type == 'remove') {
                        self.removeClass('product-item-wishlist-show');
                        self.data('process-type', 'add');
                    }
                }
            });
        } else {
            Swal.fire({
                text: "Harap Login Terlebih Dahulu",
                icon: 'warning',
                showCloseButton: true,
                showCancelButton: true,
                focusConfirm: true,
                confirmButtonText:
                    'Login',
                cancelButtonText:
                    'Kembali',
            }).then((result) => {
                if (result.isConfirmed) {
                    window.location.href = route_login + "?p=" + current_url_enc;
                }
            });
        }

    });
}

function select2_run() {
    $('.select2').select2();
}

function address_change() {

    // $('[name="id_city"]').prop('disabled', true);
    // $('[name="id_subdistrict"]').prop('disabled', true);

    $('[name="id_province"]').change(function () {
        var id_province = $(this).val();
        var route_city_get = $('body').data('route-city-get');
        var data_send = {
            id_province: id_province
        };

        $.ajax({
            url: route_city_get,
            type: 'POST',
            data: data_send,
            beforeSend: loading_start(),
            success: function (json) {
                loading_finish();
                $('[name="id_city"]').html('').select2();
                $('[name="id_subdistrict"]').html('').select2();

                $('[name="id_city"]').append($('<option>', {value: "", text: "Pilih Kota"}));

                $('[name="id_city"]').select2({data: json});
                $('[name="id_city"]').prop('disabled', false);
            }
        });
    });

    $('[name="id_city"]').change(function () {
        var id_city = $(this).val();
        var route_subdistrict_get = $('body').data('route-subdistrict-get');
        var data_send = {
            id_city: id_city
        };

        $.ajax({
            url: route_subdistrict_get,
            type: 'POST',
            data: data_send,
            beforeSend: loading_start(),
            success: function (json) {
                loading_finish();
                $('[name="id_subdistrict"]').html('').select2();

                $('[name="id_subdistrict"]').append($('<option>', {value: "", text: "Pilih Kecamatan"}));
                $('[name="id_subdistrict"]').select2({data: json});
                $('[name="id_subdistrict"]').prop('disabled', false);
            }
        });
    });
}

function member_address_primary() {
    $('.btn-member-address-primary').click(function() {
        var id_member_address = $(this).data('id-member-address');
        var route_member_address_primary = $('body').data('route-member-address-primary');
        var data_send = {
            id_member_address: id_member_address
        };

        $.ajax({
            beforeSend: loading_start(),
            url: route_member_address_primary,
            type: 'post',
            data: data_send,
            success: function() {
                window.location.reload();
            }
        });
    });
}

function member_address_delete() {
    $('.btn-member-address-delete').click(function() {

        Swal.fire({
            title: 'Yakin Hapus Data ?',
            text: "Data Alamat akan terhapus secara permanen",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Okay',
            cancelButtonText: 'Batal'
        }).then((result) => {
            if (result.isConfirmed) {
                var id_member_address = $(this).data('id-member-address');
                var route_member_address_delete = $('body').data('route-member-address-delete');
                var data_send = {
                    id_member_address: id_member_address
                };

                $.ajax({
                    beforeSend: loading_start(),
                    url: route_member_address_delete,
                    type: 'post',
                    data: data_send,
                    success: function() {
                        loading_finish();
                        window.location.reload();
                    }
                });
            }
        });


    });
}

function member_address_edit() {
    $('.btn-member-address-edit').click(function() {
        var id_member_address = $(this).data('id-member-address');
        var route_member_address_edit = $('body').data('route-member-address-edit');
        var data_send = {
            id_member_address: id_member_address
        };

        $.ajax({
            beforeSend: loading_start(),
            url: route_member_address_edit,
            type: 'post',
            data: data_send,
            success: function(json) {
                var data = JSON.parse(json);
                loading_finish();

                $.each(data.province, function(key, val) {
                    $('#modal-address-edit [name="id_province"]').append($('<option>', {value: val.id, text: val.text}));
                });
                $.each(data.city, function(key, val) {
                    $('#modal-address-edit [name="id_city"]').append($('<option>', {value: val.id, text: val.text}));
                });
                $.each(data.subdistrict, function(key, val) {
                    $('#modal-address-edit [name="id_subdistrict"]').append($('<option>', {value: val.id, text: val.text}));
                });

                $('#modal-address-edit [name="id"]').val(data.member_address.id);
                $('#modal-address-edit [name="owner_name"]').val(data.member_address.owner_name);
                $('#modal-address-edit [name="phone"]').val(data.member_address.phone);
                $('#modal-address-edit [name="id_province"]').val(data.member_address.id_province);
                $('#modal-address-edit [name="id_city"]').val(data.member_address.id_city);
                $('#modal-address-edit [name="id_subdistrict"]').val(data.member_address.id_subdistrict);
                $('#modal-address-edit [name="address"]').val(data.member_address.address);
                $('#modal-address-edit [name="primary"]').val(data.member_address.primary);

                $('#modal-address-edit').modal('show');
            }
        });

    });
}

function ship_change() {
    $('.ship-item').click(function() {
        var ship_cost = $(this).data('ship-cost');
        var cart_price_total = $('body').data('cart-price-total');
        var currency = $('body').data('currency');

        var total = format_number(parseFloat(ship_cost) + parseFloat(cart_price_total));
        $('.checkout-total').html(currency+" "+total);
        $('.ship-cost').html(currency+" "+format_number(ship_cost));

        $('[name="courier_code"]').attr('checked', false);
        $(this).children('label').children('input').attr('checked', true);


    });
}

function midtrans_payment() {

    $('.btn-midtrans-pay').click(function () {

        var ship_cost = $('[name="courier_code"]:checked').val();
        var route_courier_change_price = $('body').data('route-courier-change-price');
        var route_checkout_success = $('body').data('route-checkout-success');
        var route_checkout_pending = $('body').data('route-checkout-pending');
        var route_checkout_error = $('body').data('route-checkout-error');
        var route_payment_success = $('body').data('route-payment-success');
        var route_payment_pending = $('body').data('route-payment-pending');
        var route_payment_error = $('body').data('route-payment-error');

        $.ajax({
            beforeSend: loading_start(),
            url: route_courier_change_price,
            type: 'POST',
            data: {
                ship_cost: ship_cost
            },
            success: function(json) {
                loading_finish();

                var data = JSON.parse(json);
                var snap_token = data.snap_token;

                snap.pay(snap_token, {

                    onSuccess: function (result) {
                        loading_start();
                        $.ajax({
                            url: route_checkout_success,
                            type: 'POST',
                            data: {},
                            success: function (result) {
                                loading_finish();

                                Swal.fire({
                                    title: 'Pembayaran Sukses',
                                    text: 'Pemesanan Produk dan Transaksi Pembayaran Sukses dilakukan, Terima Kasih',
                                    icon: 'success',
                                    confirmButtonText: 'Baik'
                                }).then(function (result) {
                                    if (result.value) {
                                        window.location.href = route_payment_success
                                    }
                                });
                            }
                        });
                    },

                    onPending: function (result) {
                        loading_start();

                        $.ajax({
                            url: route_checkout_pending,
                            type: 'POST',
                            data: {},
                            success: function (result) {
                                loading_finish();

                                Swal.fire({
                                    title: 'Pemesanan Berhasil',
                                    text: 'Silahkan melakukan pembayaran sesuai metode pembayaran yang Anda lakukan, Terima Kasih',
                                    icon: 'success',
                                    confirmButtonText: 'Baik'
                                }).then(function (result) {
                                    if (result.value) {
                                        window.location.href = route_payment_pending;
                                    }
                                });


                            }
                        });
                    },

                    onError: function (result) {
                        loading_start();
                        $.ajax({
                            url: route_checkout_error,
                            type: 'POST',
                            data: {},
                            success: function (result) {
                                loading_finish();

                                Swal.fire({
                                    title: 'Terjadi Kesalahan',
                                    text: 'Harap hubungi Kami untuk terjadi kesalah ini, Terima Kasih',
                                    icon: 'success',
                                    confirmButtonText: 'Baik'
                                }).then(function (result) {
                                    if (result.value) {
                                        window.location.href = route_payment_error;
                                    }
                                });


                            }
                        });
                    }
                });
            }
        });


    });
}

function form_send() {
    $('.form-send').submit(function (e) {
        e.preventDefault();

        var alert_modal = $(this).data('alert-modal');

        loading_start();

        $.ajax({
            url: $(this).attr('action'),
            type: $(this).attr('method'),
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData: false,
            success: function (json) {
                loading_finish();
                var data = JSON.parse(json);
                $('.form-group').removeClass('validated');
                $('.invalid-feedback').remove();
                $('.form-control').removeClass('is-invalid');
                if (data.status === 'error') {

                    if (alert_modal) {
                        swal.fire({
                            position: 'center',
                            icon: 'warning',
                            title: data.title,
                            text: data.message,
                        });
                    }

                    $.each(data.errors, function (field, message) {
                        if (message) {
                            $('[name=' + field + ']').parents('.form-group').addClass('validated');
                            $('[name=' + field + ']').after('<div class="invalid-feedback">' + message + '</div>');
                        }
                    })
                } else if (data.status == 'success') {
                    if (alert_modal) {
                        swal.fire({
                            position: 'center',
                            icon: 'success',
                            title: data.title,
                            text: data.message,
                        }).then((result) => {
                            window.location.reload();
                        });
                    } else {
                        window.location.reload();
                    }
                } else {
                    swal.fire({
                        position: 'center',
                        icon: 'warning',
                        title: 'Ada kesalahan',
                    });
                }
            }
        });

        return false;
    });
}


/**
 * Old
 */


$('.btn-produk-filter').click(function () {
    var id_product_category = {};
    var category_title = '?category=';
    var no = 0;

    $('[name="id_product_category[]"]:checked').each(function () {
        // console.log($(this).val());
        id_product_category[no] = $(this).val();

        category_title += $(this).data('title') + ',';
        no++;
    });

    category_title = category_title.substring(0, category_title.length - 1);

    // console.log(id_product_category, category_title);

    window.location.href = site_url + 'produk/' + category_title;
});
