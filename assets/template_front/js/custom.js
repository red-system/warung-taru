$('#contact-form').submit(function(e) {
    e.preventDefault();
    loading_start();
    $.post($('#contact-form').attr('action'), $(this).serialize(), function(data){
        loading_finish();
        if(data.status === 'error') {
            Object.keys(data.errors).forEach(function(key) {
               $('#' + key + '-form .invalid-feedback').html(data.errors[key]);
            });
        } else {
            Swal.fire({
                title : data.title,
                text: data.message,
                icon: data.icon,
                });
            $('input[type="text"]').val('');
            $('input[type="tel"]').val('');
            $('textarea').val('');
            $('.invalid-feedback').html('');
        }
    });
    return false;
});

function loading_start() {
    $('.container-loading').removeClass('d-none').fadeIn('fast');
}

function loading_finish() {
    $('.container-loading').fadeOut('fast').addClass('d-none');
}