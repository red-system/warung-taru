<section class="page-banner-section">
    <div class="container">
        <h1>Menu</h1>
        <span>Take a look our delicious menu</span>
    </div>
</section>
<div class="container">
    <ul class="page-ban-list">
        <li><a href="<?= base_url('') ?>">Home</a></li>
        <li><a href="<?= base_url('menu') ?>">Menu</a></li>
    </ul>
</div>
<section class="menu-section style-two">
    <?php foreach($products as $row) :?>
    <div id="<?= $row->slug ?>">&nbsp;</div>
    <div class="menu-box"">
        <div class="container">
            <div class="menu-title menu-title-custom">
                <h2><?= $row->title ?></h2>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="menu-holder">
                        <ul class="menu-list">
                            <?php for($i = 0; $i < round(count($row->product) / 2); $i++) : ?>
                            <a href="<?= base_url('upload/images/' . $row->product[$i]->thumbnail) ?>" class="product zoom">
                            <li>
                                <div class="img-box">
                                    <img src="<?= base_url('upload/images/' . $row->product[$i]->thumbnail) ?>" alt="<?= $row->product[$i]->thumbnail_alt ?>">
                                </div>
                                <div class="menu-cont">
                                    <h4>
                                        <span class="title-menu"><?= $row->product[$i]->title ?></span>
                                        <span class="price">Rp. <?= number_format($row->product[$i]->price,0,",",".") ?></span>
                                    </h4>
                                    <p><?= $row->product[$i]->title_sub ?></p>
                                </div>
                            </li>
                            </a>
                            <?php endfor ?>
                        </ul>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="menu-holder">
                        <ul class="menu-list">
                        <?php for($i = round(count($row->product) / 2); $i < count($row->product); $i++) : ?>
                            <a href="<?= base_url('upload/images/' . $row->product[$i]->thumbnail) ?>" class="product zoom">
                            <li>
                                <div class="img-box">
                                    <img src="<?= base_url('upload/images/' . $row->product[$i]->thumbnail) ?>" alt="<?= $row->product[$i]->thumbnail_alt ?>">
                                </div>
                                <div class="menu-cont">
                                    <h4>
                                        <span class="title-menu"><?= $row->product[$i]->title ?></span>
                                        <span class="price">Rp. <?= number_format($row->product[$i]->price,0,",",".") ?></span>
                                    </h4>
                                    <p><?= $row->product[$i]->title_sub ?></p>
                                </div>
                            </li>
                            </a>
                        <?php endfor; ?>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php endforeach;?>
</section>