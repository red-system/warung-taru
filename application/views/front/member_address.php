<main class="bg_gray">
    <div class="container margin_30">
        <div class="page_header">
            <div class="breadcrumbs">
                <ul>
                    <li><a href="<?php echo site_url() ?>">Beranda</a></li>
                    <li>Daftar Alamat Pengiriman</li>
                </ul>
            </div>
            <h1>Daftar Alamat Pengiriman</h1>
            <br/>
            <button type="button" class="btn_1" data-toggle="modal" data-target="#modal-address-add">
                <i class="ti-plus"></i> Tambah Alamat Baru
            </button>
        </div>

        <div class="row" style="margin-top: -20px !important;">
            <div class="col-xl-12 col-lg-12 col-md-12">
                <div class="box_account">
                    <div class="form_container">
                        <?php foreach($address_list as $row) { ?>
                            <div class="row">
                                <div class="col-12 col-lg-2">Nama Pemilik/Toko</div>
                                <div class="col-12 col-lg-8">
                                    <strong><?php echo $row->owner_name ?></strong>
                                    <?php if($row->primary == 'yes') { ?>
                                        <button type="button" class="btn_1 btn-sm btn-success">Utama</button>
                                    <?php } ?>
                                </div>
                                <div class="col-12 col-lg-2"></div>
                                <div class="col-12 col-lg-2">Telepon</div>
                                <div class="col-12 col-lg-8"><?php echo $row->phone ?></div>
                                <div class="col-12 col-lg-2"></div>
                                <div class="col-12 col-lg-2">Alamat</div>
                                <div class="col-12 col-lg-8">
                                    <?php echo $row->address ?>
                                    <?php echo $row->city_name ?> - <?php echo $row->subdistrict_name ?>
                                    <?php echo $row->province_name ?>
                                    <?php echo $row->postal_code ?>
                                </div>
                                <div class="col-12 col-lg-2"></div>
                                <div class="col-12 col-lg-2"></div>
                                <div class="col-12 col-lg-8">
                                    <br/>
                                    <button type="button" class="btn_1 btn-sm btn-member-address-edit" data-id-member-address="<?php echo $row->id ?>">Edit</button>
                                    <button type="button" class="btn_1 btn-sm btn-member-address-delete" data-id-member-address="<?php echo $row->id ?>">Hapus</button>
                                    <?php if($row->primary == 'no') { ?>
                                        <button type="button" class="btn_1 btn-sm btn-member-address-primary" data-id-member-address="<?php echo $row->id ?>">Jadikan Utama</button>
                                    <?php } else { ?>
                                        <button type="button" class="btn_1 btn-sm btn-disabled">Jadikan Utama</button>
                                    <?php } ?>
                                </div>
                            </div>
                            <br/>
                            <hr/>
                            <br />
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>

<form action="<?php echo site_url('account/address_add') ?>" method="post" class="form-horizontal form-send" data-alert-modal="true">
    <div class="modal fade" id="modal-address-add" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <div class="row">
                        <div class="col-sm-12">
                            <h4 class="modal-title" id="exampleModalLabel">Tambah Alamat Baru</h4>
                        </div>
                    </div>
                </div>
                <div class="modal-body">
                    <div class="row form-group">
                        <label class="col-sm-4 control-label">Nama Pemilik / Toko</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" name="owner_name">
                        </div>
                    </div>
                    <div class="row form-group">
                        <label class="col-sm-4 control-label">Nomer Telepon</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" name="phone">
                        </div>
                    </div>
                    <div class="row form-group">
                        <label class="col-sm-4 control-label">Provinsi</label>
                        <div class="col-sm-8">
                            <select class="form-control select2" name="id_province">
                                <option value="">Pilih Provinsi</option>
                                <?php foreach($province as $row) { ?>
                                    <option value="<?php echo $row->id_province ?>"><?php echo $row->province_name ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                    <div class="row form-group">
                        <label class="col-sm-4 control-label">Kota</label>
                        <div class="col-sm-8">
                            <select class="form-control select2" name="id_city">
                                <option value="">Pilih Kota</option>
                            </select>
                        </div>
                    </div>
                    <div class="row form-group">
                        <label class="col-sm-4 control-label">Kecamatan</label>
                        <div class="col-sm-8">
                            <select class="form-control select2" name="id_subdistrict">
                                <option value="">Pilih Kecamatan</option>
                            </select>
                        </div>
                    </div>
                    <div class="row form-group">
                        <label class="col-sm-4 control-label">Nama Gedung, jalan atau lainnya</label>
                        <div class="col-sm-8">
                            <textarea class="form-control" name="address"></textarea>
                        </div>
                    </div>
                    <div class="row form-group">
                        <label class="col-sm-4 control-label">Alamat Utama Pengiriman ?</label>
                        <div class="col-sm-8">
                            <label class="radio-inline">
                                <input type="radio" name="primary" id="inlineRadio1" value="yes" checked> Ya
                            </label>
                            <label class="radio-inline">
                                <input type="radio" name="primary" id="inlineRadio2" value="no"> Tidak
                            </label>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn_1">Simpan Alamat</button>
                    <button type="button" class="btn_1 gray" data-dismiss="modal">Batal</button>
                </div>
            </div>
        </div>
    </div>
</form>


<form action="<?php echo site_url('account/address_update') ?>" method="post" class="form-horizontal form-send" data-alert-modal="true">
    <div class="modal fade" id="modal-address-edit" role="dialog" aria-labelledby="myModalLabel">

        <input type="hidden" name="id" value="">

        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <div class="row">
                        <div class="col-sm-12">
                            <h4 class="modal-title" id="exampleModalLabel">Edit Alamat</h4>
                        </div>
                    </div>
                </div>
                <div class="modal-body">
                    <div class="row form-group">
                        <label class="col-sm-4 control-label">Nama Pemilik / Toko</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" name="owner_name">
                        </div>
                    </div>
                    <div class="row form-group">
                        <label class="col-sm-4 control-label">Nomer Telepon</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" name="phone">
                        </div>
                    </div>
                    <div class="row form-group">
                        <label class="col-sm-4 control-label">Provinsi</label>
                        <div class="col-sm-8">
                            <select class="form-control select2" name="id_province">
                                <option value="">Pilih Provinsi</option>
                            </select>
                        </div>
                    </div>
                    <div class="row form-group">
                        <label class="col-sm-4 control-label">Kota</label>
                        <div class="col-sm-8">
                            <select class="form-control select2" name="id_city">
                                <option value="">Pilih Kota</option>
                            </select>
                        </div>
                    </div>
                    <div class="row form-group">
                        <label class="col-sm-4 control-label">Kecamatan</label>
                        <div class="col-sm-8">
                            <select class="form-control select2" name="id_subdistrict">
                                <option value="">Pilih Kecamatan</option>
                            </select>
                        </div>
                    </div>
                    <div class="row form-group">
                        <label class="col-sm-4 control-label">Nama Gedung, jalan atau lainnya</label>
                        <div class="col-sm-8">
                            <textarea class="form-control" name="address"></textarea>
                        </div>
                    </div>
                    <div class="row form-group">
                        <label class="col-sm-4 control-label">Alamat Utama Pengiriman ?</label>
                        <div class="col-sm-8">
                            <label class="radio-inline">
                                <input type="radio" name="primary" id="inlineRadio1" value="yes" checked> Ya
                            </label>
                            <label class="radio-inline">
                                <input type="radio" name="primary" id="inlineRadio2" value="no"> Tidak
                            </label>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn_1">Perbarui Alamat</button>
                    <button type="button" class="btn_1 gray" data-dismiss="modal">Batal</button>
                </div>
            </div>
        </div>
    </div>
</form>