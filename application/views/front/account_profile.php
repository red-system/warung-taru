<main class="bg_gray">

    <div class="container margin_30">
        <div class="page_header">
            <div class="breadcrumbs">
                <ul>
                    <li><a href="<?php echo site_url() ?>">Beranda</a></li>
                    <li>Profil</li>
                </ul>
            </div>
            <h1>Profil Saya</h1>
        </div>

        <div class="row justify-content-center">
            <div class="col-xl-4 col-lg-4 col-md-4 col-12">
                <div class="box_account">
                    <div class="form_container">
                        <img src="<?php echo $member->picture ?>" class="img-responsive">
                    </div>
                </div>
            </div>

            <div class="col-xl-8 col-lg-8 col-md-8 col-12">
                <div class="box_account">
                    <div class="form_container">
                        <form action="<?php echo site_url('account/profile_update') ?>" method="POST" class="form-horizontal form-send">
                            <h4 class="pb-3">Data Personal</h4>
                            <div class="form-group row">
                                <label for="inputEmail3" class="col-sm-3 control-label">Nama Depan</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" name="first_name" value="<?php echo $member->first_name ?>">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="inputEmail3" class="col-sm-3 control-label">Nama Belakang</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" name="last_name" value="<?php echo $member->last_name ?>">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="inputEmail3" class="col-sm-3 control-label">Jenis Kelamin</label>
                                <div class="col-sm-9">
                                    <div class="radio">
                                        <label>
                                            <input type="radio" name="gender" value="male" <?php echo $member_gender == 'male' ? 'checked':'' ?>>
                                            Laki - Laki
                                        </label>
                                    </div>
                                    <div class="radio">
                                        <label>
                                            <input type="radio" name="gender" value="female" <?php echo $member_gender == 'female' ? 'checked':'' ?>>
                                            Perempuan
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="inputEmail3" class="col-sm-3 control-label">Tanggal Lahir</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control datepicker" name="birthday" value="<?php echo $member->birthday ?>">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="inputEmail3" class="col-sm-3 control-label">Nomer Telepon</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" name="phone" value="<?php echo $member->phone ?>">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="inputEmail3" class="col-sm-3 control-label">Alamat</label>
                                <div class="col-sm-9">
                                    <textarea class="form-control" name="address"><?php echo $member->address ?></textarea>
                                </div>
                            </div>
                            <br />
                            <hr>
                            <br />
                            <h4 class="pb-3">Data Akun</h4>
                            <div class="form-group row">
                                <label for="inputEmail3" class="col-sm-3 control-label">Email</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" name="email" value="<?php echo $member->email ?>">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="inputEmail3" class="col-sm-3 control-label">Password</label>
                                <div class="col-sm-9">
                                    <input type="password" class="form-control" name="password">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="inputEmail3" class="col-sm-3 control-label">Konfirmasi Password</label>
                                <div class="col-sm-9">
                                    <input type="password" class="form-control" name="password_conf">
                                </div>
                            </div>

                            <div class="text-center">
                                <input type="submit" value="Update Profil" class="btn_1 full-width"></div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>