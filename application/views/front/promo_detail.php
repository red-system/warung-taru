<section class="page-banner-section">
    <div class="container">
        <h1>Promo</h1>
        <span>Get Our Promo</span>
    </div>
</section>
<div class="container">
    <ul class="page-ban-list">
        <li><a href="<?= base_url('') ?>">Home</a></li>
        <li><a href="<?= base_url('promo') ?>">Promo</a></li>
        <li><?= $page->title ?></li>
    </ul>
</div>
<section class="news-section blog-page">
    <div class="container">
        <div class="row">
            <div class="col-lg-8">
                <div class="news-box">
                    <div class="single-post">
                        <img src="<?= base_url('upload/images/' . $page->thumbnail) ?>" alt="<?= $page->thumbnail_alt ?>">
                        <div class="post-content">
                            <span class="auth-spanaragraph">
                                <?= date('d F Y', strtotime($page->created_at)) ?> / <?= (int) $page->views ?> Views</a>
                            </span>
                            <h2><?= $page->title ?></h2>
                            <p class="mt-2">**Silahkan Klaim Promo Dengan Kode <b><?= $page->code ?></b> </p>
                            <?= $page->description ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="sidebar">
                    <div class="search-widget widget">
                        <form method="GET" action="<?= base_url('promo') ?>">
                            <input type="search" name="search" placeholder="Search something..."/>
                            <button type="submit">
                                <i class="fa fa-search"></i>
                            </button>
                        </form>
                    </div>
                    <div class="popular-widget widget">
                        <h3>Popular Posts</h3>
                        <ul class="popular-list">
                            <?php foreach($popular_promo as $row):
                                $link = $this->main->permalink(array('promo', $row->title)); ?>
                                <a href="<?= $link ?>" title="<?= $row->title ?>">
                                <li>
                                    <img src="<?= base_url('upload/images/' . $row->thumbnail) ?>" alt="<?= $row->thumbnail_alt ?>">
                                        <div class="side-content">
                                            <h4><?= $row->title ?></h4>
                                            <span class="span-small"><?= date('d F Y', strtotime($row->created_at)) ?></span>
                                        </div>
                                    </li>
                                </a>
                            <?php endforeach; ?>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>