<section class="page-banner-section">
    <div class="container">
        <h1>Blog</h1>
        <span>What's new from us</span>
    </div>
</section>
<div class="container">
    <ul class="page-ban-list">
        <li><a href="<?= base_url('') ?>">Home</a></li>
        <li><a href="<?= base_url('blog') ?>">Blog</a></li>
        <li><?= $page->title ?></li>
    </ul>
</div>
<section class="news-section blog-page">
    <div class="container">
        <div class="row">
            <div class="col-lg-8">
                <div class="news-box">
                    <div class="single-post">
                        <img src="<?= base_url('upload/images/' . $page->thumbnail) ?>" alt="<?= $page->thumbnail_alt ?>">
                        <div class="post-content">
                            <span class="auth-spanaragraph">
                                <?= date('d F Y', strtotime($page->created_at)) ?> / <?= (int) $page->views ?> Views</a>
                            </span>
                            <h2><?= $page->title ?></h2>
                            <?= $page->description ?>
                        </div>
                    </div>
                    <div class="other-posts row">
                        <?php if(!empty($prev_blog)): ?>
                        <a class="other-posts__prev col-12 col-md-6" href="<?= $this->main->permalink(array('blog', $prev_blog->category_title, $prev_blog->title)); ?>">
                            <i class="fa fa-chevron-left"></i>
                            <img src="<?= base_url('upload/images/' . $prev_blog->thumbnail) ?>" alt="<?= $prev_blog->thumbnail_alt ?>">
                            <div class="other-posts__prev-box">
                                <span class="other-posts__desc">Previous Post</span>
                                <h3 class="other-posts__title emphasis-text">
                                    <?= $prev_blog->title ?>
                                </h3>
                            </div>
                        </a>
                        <?php else: ?>
                            <div class="col-12 col-md-6"></div>
                        <?php endif; ?>
                        <?php if(!empty($next_blog)): ?>
                        <a class="other-posts__next col-12 col-md-6" href="<?= $this->main->permalink(array('blog', $next_blog->category_title, $next_blog->title)); ?>">
                            <i class="fa fa-chevron-right"></i>
                            <img src="<?= base_url('upload/images/' . $next_blog->thumbnail) ?>" alt="<?= $next_blog->thumbnail_alt ?>">
                            <div class="other-posts__next-box">
                                <span class="other-posts__desc">Next Post</span>
                                <h3 class="other-posts__title emphasis-text">
                                    <?= $next_blog->title ?>
                                </h3>
                            </div>
                        </a>
                        <?php else: ?>
                            <div class="col-12 col-md-6"></div>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="sidebar">
                    <div class="search-widget widget">
                        <form method="GET" action="<?= base_url('blog') ?>">
                            <input type="search" name="search" placeholder="Search something..."/>
                            <button type="submit">
                                <i class="fa fa-search"></i>
                            </button>
                        </form>
                    </div>
                    <div class="category-widget widget">
                        <h3>Categories</h3>
                        <ul class="category-list">
                            <?php foreach($category as $row): ?>
                                <li><a href="blog.html"><?= $row->title ?> <span><?= $row->total ?></span></a></li>
                            <?php endforeach; ?>
                        </ul>
                    </div>
                    <div class="popular-widget widget">
                        <h3>Popular Posts</h3>
                        <ul class="popular-list">
                            <?php foreach($popular_blog as $row):
                                $link = $this->main->permalink(array('blog', $row->category_title, $row->title)); ?>
                                <a href="<?= $link ?>" title="<?= $row->title ?>">
                                <li>
                                    <img src="<?= base_url('upload/images/' . $row->thumbnail) ?>" alt="<?= $row->thumbnail_alt ?>">
                                        <div class="side-content">
                                            <h4><?= $row->title ?></h4>
                                            <span class="span-small"><?= date('d F Y', strtotime($row->created_at)) ?></span>
                                        </div>
                                    </li>
                                </a>
                            <?php endforeach; ?>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>