<section class="page-banner-section">
    <div class="container">
        <h1>Promo</h1>
        <span>Get our promotion</span>
    </div>
</section>
<div class="container">
    <ul class="page-ban-list">
        <li><a href="<?= base_url('') ?>">Home</a></li>
        <li><a href="<?= base_url('promo') ?>">Promo</a></li>
    </ul>
</div>
<section class="news-section blog-page">
    <div class="container">
        <div class="row">
            <div class="col-lg-8">
                <div class="news-box">
                    <div class="row">
                        <?php if(empty($promo_list)) : ?>
                            <div class="col-md-12">
                                Nothing to display....
                            </div>
                        <?php endif ?>
                        <?php foreach($promo_list as $row):
                            $link = $this->main->permalink(array('promo', $row->title)); ?>
                            <div class="col-md-6">
                                <a href="<?= $link ?>">
                                    <div class="news-post">
                                        <img src="<?= base_url('upload/images/'.$row->thumbnail) ?>" alt="<?= $row->thumbnail_alt ?>">
                                        <div class="news-content">
                                            <h3><?= $row->title ?></h3>
                                            <span class="category">Code : <?= $row->code ?></span>
                                            <p class="text-dark">
                                                <?= $this->main->short_desc($row->description) ?>
                                            </p>
                                            <p class="auth-paragraph">
                                            <?= date('d F Y', strtotime($row->created_at)) ?> / <?= (int) $row->views ?> views
                                            </p>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        <?php endforeach; ?>
                    </div>
                    <div class="pagination-list-box">
                        <?= $this->pagination->create_links() ?>
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="sidebar">
                    <div class="search-widget widget">
                        <form method="GET" action="<?= base_url('promo') ?>">
                            <input type="search" name="search" placeholder="Search something..."/>
                            <button type="submit">
                                <i class="fa fa-search"></i>
                            </button>
                        </form>
                    </div>
                    <div class="popular-widget widget">
                        <h3>Popular Promo</h3>
                        <ul class="popular-list">
                            <?php foreach($popular_promo as $row):
                                $link = $this->main->permalink(array('promo', $row->title)); ?>
                                <a href="<?= $link ?>" title="<?= $row->title ?>">
                                <li>
                                    <img src="<?= base_url('upload/images/' . $row->thumbnail) ?>" alt="<?= $row->thumbnail_alt ?>">
                                        <div class="side-content">
                                            <h4><?= $row->title ?></h4>
                                            <span class="span-small"><?= date('d F Y', strtotime($row->created_at)) ?></span>
                                        </div>
                                    </li>
                                </a>
                            <?php endforeach; ?>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>