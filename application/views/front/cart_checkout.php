<main class="bg_gray">
    <div class="container margin_30">
        <div class="page_header">
            <div class="breadcrumbs">
                <ul>
                    <li><a href="<?php echo site_url() ?>">Beranda</a></li>
                    <li>Checkout</li>
                </ul>
            </div>
            <h1>Checkout </h1>
        </div>
        <div class="row">
            <div class="col-lg-4 col-md-6">
                <div class="step first">
                    <h3>1. Alamat Pengiriman</h3>
                    <div class="tab-content checkout">
                        <div class="row">
                            <div class="col-12 col-lg-12">
                                <strong><?php echo $member_address->owner_name ?></strong>
                                <?php if ($member_address->primary == 'yes') { ?>
                                    <button type="button" class="btn_1 btn-sm btn-success">Utama</button>
                                <?php } ?>
                            </div>
                            <div class="col-12 col-lg-12"><?php echo $member_address->phone ?></div>
                            <div class="col-12 col-lg-12">
                                <?php echo $member_address->address ?>
                                <?php echo $member_address->city_name ?>
                                - <?php echo $member_address->subdistrict_name ?>
                                <?php echo $member_address->province_name ?>
                                <?php echo $member_address->postal_code ?>
                            </div>
                        </div>
                        <br/>
                        <div class="text-right">
                            <a href="<?php echo site_url('account/address') ?>" class="btn_1 btn-sm"><i
                                        class="ti-pencil"></i> Ubah</a>
                        </div>
                        <hr/>
                        <br/>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6">
                <div class="step middle payments">
                    <h3>2. Metode Pengiriman</h3>
                    <ul>
                        <?php foreach ($courier as $key => $row) { ?>
                            <li>
                                <label class="container_radio"> <?php echo $row['name'] ?>
                                </label>
                                <ul style="padding-left: 30px">
                                    <?php foreach ($row['costs'] as $key2 => $row2) { ?>
                                        <li class="ship-item" data-ship-cost="<?php echo $row2['cost'][0]['value'] ?>">
                                            <label class="container_radio">
                                                <?php echo $row2['service'] ?>
                                                <small><?php echo $row2['description'] ?></small><br/>
                                                <small>Ongkos kirim
                                                    : <?php echo $this->main->currency($row2['cost'][0]['value']) ?></small><br/>
                                                <small>Estimasi waktu
                                                    : <?php echo $this->main->courier_day($row2['cost'][0]['etd']) ?></small>
                                                <input type="radio" name="courier_code"
                                                       value="<?php echo $row2['cost'][0]['value'] ?>"
                                                       <?php echo $key == 0 && $key2 == 0 ? 'checked' : '' ?>>
                                                <span class="checkmark"></span>
                                            </label>
                                        </li>
                                    <?php } ?>
                                </ul>
                            </li>
                        <?php } ?>
                    </ul>
                </div>
            </div>
            <div class="col-lg-4 col-md-6">
                <div class="step last">
                    <h3>3. Ringkasan Pemesanan Produk</h3>
                    <div class="box_general summary">
                        <h5>No Invoice : <?php echo $invoice ?></h5>
                        <hr/>
                        <?php foreach ($cart_content as $row) { ?>
                            <ul>
                                <li class="clearfix">
                                    <strong><?php echo $row['name'] ?></strong>
                                    <br/>
                                    <em> <?php echo $row['qty'] ?>
                                        x <?php echo $this->main->format_currency($row['price']) ?></em>
                                    <span><?php echo $this->main->format_currency($row['subtotal']) ?></span>
                                </li>
                            </ul>
                        <?php } ?>
                        <ul>
                            <li class="clearfix"><em><strong>Sub Total</strong></em>
                                <span><?php echo $this->main->format_currency($cart_price_total) ?></span></li>
                            <li class="clearfix"><em><strong>Ongkos Kirim</strong></em> <span
                                        class="ship-cost"><?php echo $this->main->format_currency($ship_cost) ?></span>
                            </li>

                        </ul>
                        <div class="total clearfix ">TOTAL <span
                                    class="checkout-total"><?php echo $this->main->format_currency($checkout_total) ?></span>
                        </div>

                        <button type="button" class="btn_1 full-width btn-midtrans-pay" id="pay-button">Lanjut ke
                            Pembayaran
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>

<script src="<?php echo $midtrans_js_link ?>" data-client-key="<?php echo $client_key ?>"></script>
