<!DOCTYPE html>
<html lang="<?php echo $lang_active->code ?>">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title><?php echo $page->meta_title . ' &mdash; ' . $this->main->web_name() ?></title>
    <meta name="keywords" content="<?php echo $page->meta_keywords ?>"/>
    <meta name="description" content="<?php echo $page->meta_description ?>">
    <meta name="author" content="<?php echo $author ?>">
    <meta name="revisit-after" content="2 days"/>
    <meta name="robots" content="index, follow"/>
    <meta name="rating" content="General"/>
    <meta http-equiv="charset" content="ISO-8859-1"/>
    <meta http-equiv="content-language" content="<?php echo $lang_active->code ?>"/>
    <meta name="MSSmartTagsPreventParsing" content="true"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="<?php echo base_url('') ?>assets/template_front/images/logo.png" rel="shortcut icon" type="image/x-icon"/>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('')?>assets/template_front/css/styles-bundle.min.css">
</head>
<body>
	<div id="container">
		<header>
            <?php echo $menu ?>
		</header>
		<?php echo $content ?>
		<?php echo $footer ?>
	</div>
    <div class='container-loading d-none'>
        <div class='loader'>
            <div class='loader--dot'></div>
            <div class='loader--dot'></div>
            <div class='loader--dot'></div>
            <div class='loader--dot'></div>
            <div class='loader--dot'></div>
            <div class='loader--dot'></div>
            <div class='loader--text'></div>
            <div class='loader--desc'></div>
        </div>
    </div>
	<script src="<?php echo base_url('') ?>assets/template_front/js/scripts-bundle.js"></script>
</body>

</html>