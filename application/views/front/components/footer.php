<footer>
    <div class="container">
        <div class="up-footer">
            <div class="row justify-content-md-between">
                <div class="col-lg-7 col-md-6">
                    <div class="footer-widget">
                        <a href="index.html">
                            <img width="150" src="<?php echo base_url()?>assets/template_front/images/logo.png" alt="">
                        </a>
                        <p><?= $description_site ?></p>
                        <ul class="social-list">
                            <li><a target="_blank" href="<?= $facebook_link ?>"><i class="fa fa-facebook"></i></a></li>
                            <li><a target="_blank" href="<?= $instagram_link ?>"><i class="fa fa-instagram"></i></a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6">
                    <div class="footer-widget contact-widget">
                        <h3>How To Find Us</h3>
                        <p><i class="fa fa-map-marker"></i><?= $address ?></p>
                        <p><i class="fa fa-phone"></i><?= $phone ?></p>
                        <p><i class="fa fa-envelope"></i><?= $email ?></p>
                    </div>
                </div>
            </div>
        </div>
        <p class="copyright-line">Copyright © 2020 - <?= date('Y') ?> <?= $web_name ?> &bull; <?= $this->main->credit() ?></p>
        <a href="#" class="go-top">Back To Top</a>
    </div>
</footer>