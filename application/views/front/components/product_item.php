
    <div class="grid_item">
        <?php if ($row->new_status == 'yes') { ?>
            <span class="ribbon off">New</span>
        <?php } ?>
        <figure>
            <a href="<?php echo $link ?>">
                <img class="img-fluid lazy"
                     src="<?php echo $this->main->image_preview_url($row->product_thumbnail) ?>"
                     alt="">
            </a>
        </figure>
        <div class="rating">
            <?php echo $this->main->format_star($row->star) ?>
        </div>
        <a href="<?php echo $link ?>">
            <h3><?php echo $row->title ?></h3>
        </a>
        <div class="price_box">
            <span class="new_price"><?php echo $this->main->format_currency($row->price) ?></span>
            <?php if ($row->price_old) { ?>
                <span class="old_price"><?php echo $this->main->format_currency($row->price_old) ?></span>
            <?php } ?>
        </div>
        <ul>
            <?php if($wishlist_member_status) { ?>
                <li class="product-item-wishlist-show wishlist-process"
                    data-id="<?php echo $row->id ?>"
                    data-process-type="remove">
                    <a href="#"
                       class="tooltip-1"
                       data-toggle="tooltip"
                       data-placement="left"
                       title="Hapus dari wishlist">
                        <i class="ti-heart"></i>
                        <span>Hapus dari wishlist</span>
                    </a>
                </li>
            <?php } else { ?>
                <li class="wishlist-process"
                    data-id="<?php echo $row->id ?>"
                    data-process-type="add">
                    <a href="#"
                       class="tooltip-1"
                       data-toggle="tooltip"
                       data-placement="left"
                       title="Tambah ke wishlist">
                        <i class="ti-heart"></i>
                        <span>Tambah ke wishlist</span>
                    </a>
                </li>
            <?php } ?>

            <li class="btn_add_to_cart">
                <a href="javascript:;"
                   class="tooltip-1"
                   data-toggle="tooltip"
                   data-placement="left"
                   title="Masukkan Keranjang"
                   data-id="<?php echo $row->id ?>"
                   data-title="<?php echo $row->title ?>"
                   data-category-title="<?php echo $row->category_title ?>"
                   data-image="<?php echo $this->main->image_preview_url($row->product_thumbnail) ?>"
                   data-price="<?php echo $row->price ?>"
                   data-price-label="<?php echo $this->main->format_currency($row->price) ?>"
                    <?php if ($row->price_old) { ?>
                        data-price-old="<?php echo $row->price_old ?>"
                        data-price-old-label="<?php echo $this->main->format_currency($row->price_old) ?>"
                    <?php } ?>>
                    <i class="ti-shopping-cart"></i><span>Masukkan Keranjang</span>
                </a>
            </li>
        </ul>
    </div>