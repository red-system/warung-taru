<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <div class="container">
        <a class="navbar-brand" href="<?= base_url('') ?>">
            <img width="150" src="<?php echo base_url('assets/template_front/images/logo.png') ?>" alt="Logo">
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#menu" aria-controls="menu" aria-expanded="false" aria-label="Toggle navigation">
        <i class="fa fa-fw fa-bars"></i>
        </button>
        <div class="collapse navbar-collapse" id="menu">
            <ul class="navbar-nav m-auto">
                <li>
                    <a class="<?= $page->type == 'home' ? 'active' : '' ?>" href="<?= base_url('') ?>">Beranda</a>
                </li>
                <li class="drop-link">
                    <a class="<?= $page->type == 'product' ? 'active' : '' ?>" href="<?= base_url('menu') ?>">Menu <i class="fa fa-angle-down"></i></a>
                    <ul class="dropdown">
                        <?php foreach($category_menu as $row): ?>
                            <li>
                                <a href="<?= base_url('menu#' . $row->slug) ?>"><?= $row->title ?></a>
                            </li>
                        <?php endforeach ?>
                    </ul>
                </li>
                <li>
                    <a class="<?= $page->type == 'blog' ? 'active' : '' ?>" href="<?= base_url('blog') ?>">Blog</a>
                </li>
                <li>
                    <a class="<?= $page->type == 'promo' ? 'active' : '' ?>" href="<?= base_url('promo') ?>">Promo</a>
                </li>
                <li>
                    <a class="<?= $page->type == 'gallery_photo' ? 'active' : '' ?>" href="<?= base_url('galeri-foto') ?>">Galeri</a>
                </li>
                <li>
                    <a class="<?= $page->type == 'contact_us' ? 'active' : '' ?>" href="<?= base_url('kontak-kami') ?>">Kontak Kami</a>
                </li>
            </ul>
        </div>
        <a href="<?= $phone_link ?>" class="button-one"><i class="fa fa-phone"></i> <span class="ml-1"><?= $phone ?></span> </a>
    </div>
</nav>