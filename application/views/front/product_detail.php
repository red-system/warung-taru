<main>
    <br/>
    <br/>
    <div class="container margin_30">
        <div class="row">
            <div class="col-md-6">
                <div class="all">
                    <div class="slider">
                        <div class="owl-carousel owl-theme main">
                            <?php foreach ($product_gallery as $row) { ?>
                                <div style="background-image: url('<?php echo $this->main->image_preview_url($row->thumbnail) ?>');"
                                     class="item-box"></div>
                            <?php } ?>
                        </div>
                        <div class="left nonl"><i class="ti-angle-left"></i></div>
                        <div class="right"><i class="ti-angle-right"></i></div>
                    </div>
                    <div class="slider-two">
                        <div class="owl-carousel owl-theme thumbs">
                            <?php foreach ($product_gallery as $key => $row) { ?>
                                <div style="background-image: url('<?php echo $this->main->image_preview_url($row->thumbnail) ?>');"
                                     class="item <?php echo $key == 0 ? 'active' : '' ?>"></div>
                            <?php } ?>
                        </div>
                        <div class="left-t nonl-t"></div>
                        <div class="right-t"></div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="breadcrumbs">
                    <ul>
                        <li><a href="<?php echo site_url() ?>">Beranda</a></li>
                        <li><a href="<?php echo site_url('produk') ?>">Produk</a></li>
                        <li>
                            <a href="<?php echo $this->main->permalink(array('produk', $page->category_title)) ?>"><?php echo $page->category_title ?></a>
                        </li>
                        <li><?php echo $page->title ?></li>
                    </ul>
                </div>

                <div class="prod_info">
                    <h1><?php echo $page->title ?></h1>
                    <div class="rating">
                        <?php echo $this->main->format_star($page->star) ?>
                    </div>
                    <?php echo $page->description ?>
                    <div class="prod_options">
                        <div class="row">
                            <label class="col-xl-6 col-lg-6  col-md-6 col-6"><strong>Quantity</strong></label>
                            <div class="col-xl-4 col-lg-5 col-md-6 col-6">
                                <div class="numbers-row">
                                    <input type="text" value="1" id="quantity_1" class="qty2" name="product_qty">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-5 col-md-6">
                            <div class="price_main">
                                <span class="new_price"><?php echo $this->main->format_currency($page->price) ?></span>
                                <?php if ($page->price_old) { ?>
                                    <span class="old_price"><?php echo $this->main->format_currency($page->price_old) ?></span>
                                <?php } ?>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-8">
                            <div class="btn_add_to_cart">
                                <a href="javascript:;"
                                   class="btn_1"
                                   data-id="<?php echo $page->id ?>"
                                   data-title="<?php echo $page->title ?>"
                                   data-category-title="<?php echo $page->category_title ?>"
                                   data-image="<?php echo $this->main->image_preview_url($page->product_thumbnail) ?>"
                                   data-price="<?php echo $page->price ?>"
                                   data-price-label="<?php echo $this->main->format_currency($row->price) ?>"
                                    <?php if ($page->price_old) { ?>
                                        data-price-old="<?php echo $row->price_old ?>"
                                        data-price-old-label="<?php echo $this->main->format_currency($row->price_old) ?>"
                                    <?php } ?>>
                                    Masukkan Keranjang
                                </a>
                            </div>

                            <div class="product_actions">
                                <ul>
                                    <li>
                                        <a href="javascript:;"><i class="ti-heart"></i><span>Add to Wishlist</span></a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <hr/>
    </div>

    <div class="container margin_60_35">
        <div class="main_title">
            <h2>Produk Berkaitan</h2>
            <span>Daftar</span>
            <p>Berikut adalah daftar produk berkaitan dengan produk <?php echo $page->title ?></p>
        </div>
        <div class="owl-carousel owl-theme products_carousel">

            <?php foreach ($related as $row) {
                $product_item_view = $this->main->product_item_view($row);?>
                <div class="item">
                    <?php echo $product_item_view ?>
                </div>
            <?php } ?>
        </div>
    </div>

    <div class="feat">
        <div class="container">
            <ul>
                <li>
                    <div class="box">
                        <i class="ti-gift"></i>
                        <div class="justify-content-center">
                            <h3>Free Shipping</h3>
                            <p>For all oders over $99</p>
                        </div>
                    </div>
                </li>
                <li>
                    <div class="box">
                        <i class="ti-wallet"></i>
                        <div class="justify-content-center">
                            <h3>Secure Payment</h3>
                            <p>100% secure payment</p>
                        </div>
                    </div>
                </li>
                <li>
                    <div class="box">
                        <i class="ti-headphone-alt"></i>
                        <div class="justify-content-center">
                            <h3>24/7 Support</h3>
                            <p>Online top support</p>
                        </div>
                    </div>
                </li>
            </ul>
        </div>
    </div>
    <div class="also-bought-list hide"><?php echo $also_bought ?></div>

</main>

