<section class="page-banner-section">
    <div class="container">
        <h1>Kontak Kami</h1>
    </div>
</section>
<div class="container">
    <ul class="page-ban-list">
        <li><a href="<?= base_url('') ?>">Beranda</a></li>
        <li><a href="<?= base_url('kontak-kami') ?>">Kontak Kami</a></li>
    </ul>
</div>
<section class="contact-section">
    <div class="container">
        <div class="contact-box">
            <div class="title-section">
                <h2>Kontak Kami</h2>
            </div>
            <div class="row">
                <div class="col-md-5">
                    <div class="open-info">
                        <h3>Opening Hours:</h3>
                        <div class="info-line">
                            <i class="fa fa-clock-o"></i>
                            <?php echo $opening_hours ?>
                        </div>
                        <!-- <div class="info-line">
                            <i class="fa fa-calendar-check-o"></i>
                            <p>
                                <span>Booking Time:</span> 24/7 Hours
                            </p>
                        </div> -->
                    </div>
                    <div class="contact-info">
                        <h3>Contact Info</h3>
                        <p><i class="fa fa-map-marker"></i><?= $address ?></p>
                        <p><i class="fa fa-phone"></i><?= $phone ?></p>
                        <p><i class="fa fa-envelope"></i><?= $email ?></p>
                    </div>
                </div>
                <div class="col-md-7">
                    <form id="contact-form" action="<?= base_url('kontak-kami/kirim') ?>" method="POST">
                        <div id="name-form">
                            <label for="name">Nama:</label>
                            <input name="name" id="name" type="text" placeholder="Enter your name">
                            <div class="invalid-feedback"></div>
                        </div>
                        <div id="email-form">
                            <label for="email">Email:</label>
                            <input name="email" id="email" type="text" placeholder="Enter your email">
                            <div class="invalid-feedback"></div>
                        </div>
                        <div id="phone-form">
                            <label for="phone">Telepon:</label>
                            <input name="phone" id="phone" type="text" placeholder="Enter your phone">
                            <div class="invalid-feedback"></div>
                        </div>
                        <div id="message-form">
                            <label for="message">Pesan:</label>
                            <textarea name="message" id="message" placeholder="Enter message"></textarea>
                            <div class="invalid-feedback"></div>
                        </div>
                        <div id="captcha-form">
                            <label for="message">Kode Captcha:</label>
                            <div class="d-flex align-items-stretch">
                                <input type="text" name="captcha" class="mr-1 mr-md-3"> <?= $captcha; ?> 
                            </div>
                            <div class="invalid-feedback"></div>
                        </div>
                        
                        <input type="submit" id="submit_contact" value="Kirim Pesan">
                    </form>
                </div>
            </div>
        </div>

    </div>
</section>
<iframe scrolling="no" class="gmap"
        src="<?php echo $address_link_long ?>" style="border: 1px solid black">
</iframe>