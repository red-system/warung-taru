<section id="home-section">
    <div id="rev_slider_202_1_wrapper" class="rev_slider_wrapper fullscreen-container" data-alias="concept1">
        <div id="rev_slider_202_1" class="rev_slider fullscreenbanner" data-version="5.1.1RC">
            <ul>
                <?php foreach($slider as $row): ?>
                <li data-transition="slidingoverlaydown" data-slotamount="default" data-easein="default" data-easeout="default" data-masterspeed="default"  data-thumb="<?= base_url('upload/images/' . $row->thumbnail) ?>" data-rotate="0" data-saveperformance="off" data-title="<?= $row->title ?>" data-description="<?= $row->description ?>">
                    <img src="<?= base_url('upload/images/' . $row->thumbnail ) ?>" alt="<?= $row->thumbnail_alt ?>" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" data-bgparallax="5" class="rev-slidebg" data-no-retina>
                    <div class="tp-caption Concept-Title tp-resizeme rs-parallaxlevel-2 z-index-7 no-wrap"
                        data-x="['left','left','left','left']"
                        data-hoffset="['15','15','15','15']"
                        data-y="['middle','middle','middle','middle']"
                        data-fontsize="['90','70','56','56']"
                        data-lineheight="['86','72','48','48']"
                        data-voffset="['-10','-10','-10','-10']"
                        data-whitespace="nowrap"
                        data-transform_idle="o:1;"
                        data-transform_in="z:0;rX:0;rY:0;rZ:0;sX:0.9;sY:0.9;skX:0;skY:0;opacity:0;s:1500;e:Power3.easeInOut;"
                        data-transform_out="x:left(R);s:1000;e:Power3.easeIn;s:1000;e:Power3.easeIn;"
                        data-start="1300"
                        data-splitin="none"
                        data-splitout="none"
                        data-responsive_offset="on">
                        <?= $row->title ?>
                    </div>
                    <div class="tp-caption Concept-SubTitle tp-resizeme rs-parallaxlevel-2 z-index-6 no-wrap" 
                        data-x="['left','left','left','left']"
                        data-hoffset="['15','15','15','15']"
                        data-y="['middle','middle','middle','middle']"
                        data-voffset="['60','60','50','50']"
                        data-fontsize="['26','24','22','22']"
                        data-width="none"
                        data-height="none"
                        data-whitespace="nowrap"
                        data-transform_idle="o:1;"
                        data-transform_in="z:0;rX:0;rY:0;rZ:0;sX:0.9;sY:0.9;skX:0;skY:0;opacity:0;s:1500;e:Power3.easeInOut;"
                        data-transform_out="x:left(R);s:1000;e:Power3.easeIn;s:1000;e:Power3.easeIn;"
                        data-start="1600"
                        data-splitin="none"
                        data-splitout="none"
                        data-responsive_offset="on"><?= $row->description ?>
                    </div>
                    <div class="tp-caption Concept-SubTitle tp-resizeme rs-parallaxlevel-2 z-index-6 no-wrap" 
                        data-x="['left','left','left','left']"
                        data-hoffset="['15','15','15','15']"
                        data-y="['middle','middle','middle','middle']"
                        data-voffset="['130','120','110','100']"
                        data-fontsize="['20','20','20','20']"
                        data-width="none"
                        data-height="none"
                        data-whitespace="nowrap"
                        data-transform_idle="o:1;"
                        data-transform_in="z:0;rX:0;rY:0;rZ:0;sX:0.9;sY:0.9;skX:0;skY:0;opacity:0;s:1500;e:Power3.easeInOut;"
                        data-transform_out="x:left(R);s:1000;e:Power3.easeIn;s:1000;e:Power3.easeIn;"
                        data-start="2000"
                        data-splitin="none"
                        data-splitout="none"
                        data-responsive_offset="on"><a class="button-two" href="<?= $row->url ?>"><?= $row->button_text ?></a>
                    </div>
                </li>
                <?php endforeach; ?>
            </ul>
            <div class="tp-bannertimer tp-bottom" style="visibility: hidden !important;"></div>
        </div>
    </div>
</section>
<!-- <section class="banner-section banner-section-small-padding">
    <div class="container">
        <div class="row align-items-center justify-content-md-between">
            <div class="banner-box col-12 col-md-5 order-2 order-md-1">
                <h3>Best Dish Of The Day</h3>
                <h2><?= $best_dish_today->title ?></h2>
                <p class="paragraph">
                    <?= $best_dish_today->title_sub ?>
                </p>
                <span class="price">
                    Rp. <?= number_format($best_dish_today->price,0,",",".") ?>
                </span>
                <a href="menu.html" class="button-two">View This Menu</a>
            </div>
            <img class="col-12 col-md-5 mb-3 order-1 order-md-2 mb-md-0" 
                src="<?php echo base_url('upload/images/' . $best_dish_today->thumbnail) ?>" 
                alt="<?= $best_dish_today->thumbnail_alt ?>">
        </div>
    </div>
</section> -->
<section class="popular-menu-section">
    <div class="container">
        <div class="title-section">
            <h2>Popular Dishes</h2>
            <span>Try the delicious new dishes from our chef.</span>
        </div>
        <div class="popular-menu-box owl-wrapper">
            <div class="owl-carousel" data-num="3">
                <?php foreach($popular_dishes as $row): ?>
                    <div class="item">
                        <div class="popular-menu-post">
                            <a href="<?php echo base_url('upload/images/' . $row->thumbnail) ?>" class="zoom">
                            <img src="<?php echo base_url('upload/images/' . $row->thumbnail) ?>"
                                alt="<?= $row->thumbnail_alt ?>">
                            </a>
                            <h3><?= $row->title ?></h3>
                            <p><?= $row->title_sub ?></p>
                            <span class="price">Rp. <?= number_format($row->price,0,",",".") ?></span>
                            <!-- <a href="menu.html" class="button-one">View</a> -->
                        </div>
                    </div>
                <?php endforeach; ?>
            </div>
        </div>
    </div>
</section>
<section class="services-section">
    <div class="container">
        <div class="services-box">
            <div class="row">
                <?php foreach($service as $row): ?>
                    <div class="col-lg-3 col-md-6">
                        <div class="services-post">
                            <img class="service-icon" src="<?php echo base_url('upload/images/' . $row->thumbnail) ?>" alt="<?= $row->thumbnail_alt ?>">
                            <h3><?= $row->title ?></h3>
                            <?= $row->description ?>
                        </div>
                    </div>
                <?php endforeach; ?>
            </div>
        </div>
    </div>
</section>
<section class="banner-section2">
    <div class="container">
        <div class="banner-box">
            <h2>Menu</h2>
            <a href="<?= base_url('menu') ?>" class="button-two">View Our Menu</a>
        </div>
    </div>
</section>
<section class="menu-section">
    <?php foreach($products as $key => $row): ?>
        <div class="menu-box">
            <div class="container">   
                <div class="row justify-content-md-between">
                    <div class="col-lg-5 col-md-6 order-1 <?= ($key % 2 ) ? 'order-md-2' : '' ?>">
                        <div class="image-holder">
                            <img src="<?php echo base_url('upload/images/' . $row->thumbnail) ?>" alt="<?= $row->thumbnail_alt ?>">
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 order-2 <?= ($key % 2 ) ? 'order-md-1' : '' ?>">
                        <div class="menu-holder">
                            <h2><?= $row->title ?></h2>
                            <ul class="menu-list">
                            <?php foreach($row->product as $item) : ?>
                                <a class="product zoom" href="<?php echo base_url('upload/images/' . $item->thumbnail) ?>" target="_blank" rel="noopener noreferrer">
                                <li>
                                    <div class="img-box">
                                        <img class="zoom" src="<?php echo base_url('upload/images/' . $item->thumbnail) ?>" alt="<?= $item->thumbnail_alt ?>">
                                    </div>
                                    <div class="menu-cont">
                                        <h4>
                                            <span class="title-menu"><?= $item->title ?></span>
                                            <span class="price">Rp. <?= number_format($item->price,0,",",".") ?></span>
                                        </h4>
                                        <p><?= $item->title_sub ?></p>
                                    </div>
                                </li>
                                </a>
                            <?php endforeach; ?>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <?php endforeach; ?>
</section>
<section class="testimonial-section">
    <div class="container">
        <div class="title-section">
            <h2>Testimonials</h2>
            <span>What They Say About Us</span>
        </div>
        <div class="testimonial-box owl-wrapper">
            <div class="owl-carousel" data-num="3">
                <?php foreach($testimonials as $row) : ?>
                    <div class="item">
                    <div class="testimonial-post">
                        <img src="<?php echo base_url('upload/images/' . $row->thumbnail) ?>" alt="<?= $row->thumbnail_alt ?>">
                        <h3><?= $row->title ?></h3>
                        <span>Happy Customer</span>
                        <p>“ <?= $row->description ?> ”</p>
                    </div>
                </div>
                <?php endforeach; ?>
            </div>
        </div>
    </div>
</section>
<section class="gallery-section">
    <?php foreach($galleries as $row): ?>
        <a href="<?= base_url('upload/images/' . $row->thumbnail) ?>" title="<?= $row->thumbnail_alt ?>" class="gallery-zoom">
            <img src="<?= base_url('upload/images/' . $row->thumbnail) ?>" alt="<?= $row->thumbnail_alt ?>">
        </a>
    <?php endforeach; ?>
</section>
<section class="news-section">
    <div class="container">
        <div class="title-section white-style">
            <h2>Latest Blog</h2>
            <span class="text-white">What's new in our restaurant</span>
        </div>
        <div class="news-box">
            <div class="row">
                <?php foreach($blog_home as $row):
                    $link = $this->main->permalink(array('blog', $row->category_title, $row->title)); ?>
                        <div class="col-lg-4 col-md-6">
                        <a href="<?= $link ?>" title="<?= $row->title ?>">
                            <div class="news-post">
                                <img src="<?php echo base_url('upload/images/' . $row->thumbnail) ?>" alt="<?= $row->thumbnail_alt ?>">
                                <div class="news-content">
                                    <h3><?= $row->title ?></h3>
                                    <p class="text-dark">
                                        <?= $this->main->short_desc($row->description) ?>
                                    </p>
                                    <p class="auth-paragraph">
                                        <?= date('d F Y', strtotime($row->created_at)) ?> / <?= (int) $row->views ?> views
                                    </p>
                                </div>
                            </div>
                        </a>
                        </div>
                <?php endforeach; ?>
            </div>
            <div class="center-button">
                <a href="<?= base_url('blog') ?>" class="button-one">View All Blog</a>
            </div>
        </div>
    </div>
</section>