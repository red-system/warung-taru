<main class="bg_gray">
    <div class="bg_white">
        <div class="container margin_60_35">
            <div class="main_title">
                <h1><?php echo $page->title ?></h1>
                <span>Produk</span>
                <p><?php echo $page->title_sub ?></p>
            </div>

            <div class="col-lg-12">
                <div class="row small-gutters">
                    <?php foreach ($wishlist as $row) {
                        $product_item_view = $this->main->product_item_view($row); ?>
                        <div class="col-12 col-md-4 col-xl-3">
                            <?php echo $product_item_view ?>
                        </div>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
</main>