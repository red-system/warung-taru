<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<!--[if !mso]><!--><meta http-equiv="X-UA-Compatible" content="IE=edge" /><!--<![endif]-->
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<meta name="x-apple-disable-message-reformatting">
	<title>Email Tempalte</title>
	<style type="text/css">
		
		
			@import url(https://fonts.googleapis.com/css?family=Nunito:200,300,400,600,700,800,900|Raleway:200,300,400,500,600,700,800,900);
		
			html{width:100%}
			body{-webkit-text-size-adjust:none;-ms-text-size-adjust:none;margin:0;padding:0;font-family:'Open Sans',Arial,Sans-serif!important}
			table{border-spacing:0;table-layout:auto;margin:0 auto}
			img{display:block!important;overflow:hidden!important}
			a{text-decoration: none;color:unset}
			.ReadMsgBody{width:100%;background-color:#fff}
			.ExternalClass{width:100%;background-color:#fff}
			.ExternalClass,.ExternalClass p,.ExternalClass span,.ExternalClass font,.ExternalClass td,.ExternalClass div{line-height:100%}
			.yshortcuts a{border-bottom:none!important}
			.full{width:100%}
			.pad{width:92%}
			@media only screen and (max-width: 650px) {
			.res-pad{width:92%;max-width:92%}
			.res-full{width:100%;max-width:100%}
			.res-left{text-align:left!important}
			.res-right{text-align:right!important}
			.res-center{text-align:center!important}
			}@media only screen and (max-width: 750px) {
			.margin-full{width:100%;max-width:100%}
			.margin-pad{width:92%;max-width:92%;max-width:600px}
			}
	</style>
</head>
<body>

<table bgcolor="#F5F5F5" align="center" class="full" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td>
			<table bgcolor="#F5F5F5" width="750" align="center" class="margin-full" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td>
						<table width="600" align="center" class="margin-pad" border="0" cellpadding="0" cellspacing="0">
							<tr><td height="20" style="font-size:0px" >&nbsp;</td></tr>
							<!-- paragraph -->
							<tr >
								<td class="res-center" style="text-align: right; color: #909090; font-family: 'Nunito', Arial, Sans-serif; font-size: 15px; letter-spacing: 0.4px; line-height: 23px; word-break: break-word;" >
									Can't you see this email?
									<a href="https://example.com" style="color: #304050; font-family: 'Nunito', Arial, Sans-serif; letter-spacing: 0.7px; text-decoration: none; word-break: break-word; padding-left: 4px; font-weight: 600;">
										View in your browser
									</a>
								</td>
							</tr>
							<!-- paragraph end -->
							<tr><td height="20" style="font-size:0px" >&nbsp;</td></tr>
						</table>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
<!-- ====== Module : Intro ====== -->
<table bgcolor="#F5F5F5" align="center" class="full" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td>
			<table bgcolor="#004DDA" align="center" width="750" class="margin-full" style="background-size: cover; background-position: center; border-radius: 6px 6px 0 0;" border="0" cellpadding="0" cellspacing="0" background="<?php echo base_url() ?>assets/template_front/img/mail/module02-bg01.png">
				<tr>
					<td>
						<table width="600" align="center" class="margin-pad" border="0" cellpadding="0" cellspacing="0">
							<tr><td height="35" style="font-size:0px" >&nbsp;</td></tr>
							<!-- column x2 -->
							<tr >
								<td>
									<table class="full" align="center" border="0" cellpadding="0" cellspacing="0">
										<tr>
											<td valign="top">
												<!-- left column -->
												<table width="100" align="left" class="res-full" border="0" cellpadding="0" cellspacing="0">
													<!-- image -->
													<tr >
														<td>
															<table align="left" class="res-full" border="0" cellpadding="0" cellspacing="0">
																<tr>
																	<td>
																		<table align="center" border="0" cellpadding="0" cellspacing="0">
																			<tr>
																				<td>
																				<img width="80" style="max-width: 80px; width: 100%; display: block; line-height: 0px; font-size: 0px; border: 0px;" src="<?php echo base_url() ?>assets/template_front/img/mail/module02-img01.png">
																				</td>
																			</tr>
																		</table>
																	</td>
																</tr>
															</table>
														</td>
													</tr>
													<!-- image end -->
												</table>
												<!-- left column end -->
												<!--[if (gte mso 9)|(IE)]></td><td><![endif]-->
												<table width="1" align="left" class="res-full" border="0" cellpadding="0" cellspacing="0">
													<tr><td height="20" style="font-size:0px">&nbsp;</td></tr>
												</table>
												<!--[if (gte mso 9)|(IE)]></td><td valign="top"><![endif]-->
												<!-- right column -->
												<table width="480" align="right" class="res-full" border="0" cellpadding="0" cellspacing="0">
													<tr><td height="28" style="font-size: 0px;" >&nbsp;</td></tr>
													<!-- nested column -->
													<tr>
														<td>
															<table align="right" class="res-full" border="0" cellpadding="0" cellspacing="0">
																<tr>
																	<td>
																		<table align="center" border="0" cellpadding="0" cellspacing="0">
																			<tr>
																				<td>
																					<!--[if (gte mso 9)|(IE)]><table border="0" cellpadding="0" cellspacing="0"><tr><![endif]-->
																					<!--[if !((gte mso 9)|(IE))]-->
																					<!-- column -->
																					<table align="left" border="0" cellpadding="0" cellspacing="0">
																						<tr>
																							<!--[endif]-->
																								<!-- link -->
																								<td class="res-center" style="text-align: center; color: white; font-family: 'Raleway', Arial, Sans-serif; font-size: 15px; letter-spacing: 1.8px; line-height: 23px; word-break: break-word; padding: 0 7px;" >
																									<a href="<?php echo site_url() ?>" style="color: white; font-family: 'Nunito', Arial, Sans-serif; font-size: 15px; letter-spacing: 0.7px; text-decoration: none; word-break: break-word;" >
																										BERANDA
																									</a>
																								</td>
																								<!-- link end -->
																							<!--[if !((gte mso 9)|(IE))]-->
																						</tr>
																					</table>
																					<!-- column end -->
																					<!-- column -->
																					<table align="left" border="0" cellpadding="0" cellspacing="0">
																						<tr>
																							<!--[endif]-->
																								<!-- link -->
																								<td class="res-center" style="text-align: center; color: white; font-family: 'Raleway', Arial, Sans-serif; font-size: 15px; letter-spacing: 1.8px; line-height: 23px; word-break: break-word; padding: 0 7px;" >
																									<a href="<?php echo site_url('produk') ?>" style="color: white; font-family: 'Nunito', Arial, Sans-serif; font-size: 15px; letter-spacing: 0.7px; text-decoration: none; word-break: break-word;" >
																										DAFTAR PRODUK
																									</a>
																								</td>
																								<!-- link end -->
																							<!--[if !((gte mso 9)|(IE))]-->
																						</tr>
																					</table>
																					<!-- column end -->
																					<!-- column -->
																					<table align="left" border="0" cellpadding="0" cellspacing="0">
																						<tr>
																							<!--[endif]-->
																								<!-- link -->
																								<td class="res-center" style="text-align: center; color: white; font-family: 'Raleway', Arial, Sans-serif; font-size: 15px; letter-spacing: 1.8px; line-height: 23px; word-break: break-word; padding: 0 7px;" >
																									<a href="<?php echo site_url('galeri-foto') ?>" style="color: white; font-family: 'Nunito', Arial, Sans-serif; font-size: 15px; letter-spacing: 0.7px; text-decoration: none; word-break: break-word;" >
																										GALERI
																									</a>
																								</td>
																								<!-- link end -->
																							<!--[if !((gte mso 9)|(IE))]-->
																						</tr>
																					</table>
																					<!-- column end -->
																					<!-- column -->
																					<table align="left" border="0" cellpadding="0" cellspacing="0">
																						<tr>
																							<!--[endif]-->
																								<!-- link -->
																								<td class="res-center" style="text-align: center; color: white; font-family: 'Raleway', Arial, Sans-serif; font-size: 15px; letter-spacing: 1.8px; line-height: 23px; word-break: break-word; padding: 0 7px;" >
																									<a href="<?php echo site_url('blog') ?>" style="color: white; font-family: 'Nunito', Arial, Sans-serif; font-size: 15px; letter-spacing: 0.7px; text-decoration: none; word-break: break-word;" >
																										BLOG
																									</a>
																								</td>
																								<!-- link end -->
																							<!--[if !((gte mso 9)|(IE))]-->
																						</tr>
																					</table>
																					<!-- column end -->
																					<!-- column -->
																					<table align="left" border="0" cellpadding="0" cellspacing="0">
																						<tr>
																							<!--[endif]-->
																								<!-- link -->
																								<td class="res-center" style="text-align: center; color: white; font-family: 'Raleway', Arial, Sans-serif; font-size: 15px; letter-spacing: 1.8px; line-height: 23px; word-break: break-word; padding: 0 7px;" >
																									<a href="<?php echo site_url('kontak-kami') ?>" style="color: white; font-family: 'Nunito', Arial, Sans-serif; font-size: 15px; letter-spacing: 0.7px; text-decoration: none; word-break: break-word;" >
																										KONTAK
																									</a>
																								</td>
																								<!-- link end -->
																							<!--[if !((gte mso 9)|(IE))]-->
																						</tr>
																					</table>
																					<!-- column end -->
																					<!--[endif]-->
																					<!--[if (gte mso 9)|(IE)]></tr></table><![endif]-->
																				</td>
																			</tr>
																		</table>
																	</td>
																</tr>
															</table>
														</td>
													</tr>
													<!-- nested column end -->
												</table>
												<!-- right column end -->
											</td>
										</tr>
									</table>
								</td>
							</tr>
							<tr><td height="40" style="font-size:0px" >&nbsp;</td></tr>
						</table>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>

<table bgcolor="#F5F5F5" align="center" class="full" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td>
			<table bgcolor="white" width="750" align="center" class="margin-full" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td>
						<table width="600" align="center" class="margin-pad" border="0" cellpadding="0" cellspacing="0">
							<tr><td height="70" style="font-size:0px" >&nbsp;</td></tr>
							<!-- title -->
							<tr >
								<td class="res-center" style="text-align: center; color: #707070; font-family: 'Raleway', Arial, Sans-serif; font-size: 20px; letter-spacing: 1px; word-break: break-word;" >
									No Invoice : #<?php echo $member_cart->invoice ?>
								</td>
							</tr>
							<!-- title end -->
							<tr><td height="13" style="font-size:0px" >&nbsp;</td></tr>
							<!-- dash -->
							<tr >
								<td>
									<table align="center" class="res-full" border="0" cellpadding="0" cellspacing="0">
										<tr>
											<td>
												<table bgcolor="#006D89" align="center" style="border-radius: 10px;" border="0" cellpadding="0" cellspacing="0">
													<tr>
														<td width="55" height="3" style="font-size:0px" >&nbsp;</td>
													</tr>
												</table>
											</td>
										</tr>
									</table>
								</td>
							</tr>
							<!-- dash end -->
							<tr><td height="30" style="font-size:0px" >&nbsp;</td></tr>
							<!-- column x3 -->
							<tr>
								<td>
									<table bgcolor="#F5F5F5" align="center" class="full" border="0" cellpadding="0" cellspacing="0">
										<tr>
											<td>
												<table bgcolor="white" width="750" align="center" class="margin-full" border="0" cellpadding="0" cellspacing="0">
													<tr>
														<td>
															<table width="600" align="center" class="margin-pad" border="0" cellpadding="0" cellspacing="0">
																<tr><td height="35" style="font-size:0px" >&nbsp;</td></tr>
																<tr >
																	<td>
																		<table class="full" align="center" border="0" cellpadding="0" cellspacing="0">
																			<tr>
																				<td valign="top">
																					<table width="600" align="left" class="res-full" border="0" cellpadding="0" cellspacing="0">
                                                                                        <?php foreach($member_cart_detail as $row) {
                                                                                            $cart_price_total += $row->qty * $row->price; ?>
																						<tr>
																							<td>
																								<table style="width: 100%" align="center" border="0" cellpadding="0" cellspacing="0">
																									<tr>
																										<td width="100">
																											<table align="left" class="res-full" border="0" cellpadding="0" cellspacing="0">
																												<tr>
																													<td>
																														<table align="left" border="0" cellpadding="0" cellspacing="0">
																															<tr>
																																<td>
																																	<img width="100" style="max-width: 100px; width: 100%; display: block; line-height: 0px; font-size: 0px; border: 0px;" src="<?php echo $this->main->image_preview_url($row->thumbnail) ?>">
																																</td>
																															</tr>
																														</table>
																													</td>
																												</tr>
																											</table>
																										</td>
																										<td>
																											<table class="full" align="center" border="0" cellpadding="0" cellspacing="0">
																												<tr>
																													<td class="res-center" style="text-align: justify; font-family: 'Raleway', Arial, Sans-serif; font-size: 14px; word-break: break-word; padding: 0 10px;" >
																														<strong><?php echo $row->title ?></strong>
																													</td>
																												</tr>
																												<tr>
																													<td class="res-center" style="text-align: justify; font-family: 'Raleway', Arial, Sans-serif; font-size: 14px; word-break: break-word; padding: 0 10px;" >
																														<?php echo $row->title_sub ?>
																													</td>
																												</tr>
																											</table>
																										</td>
																										<td width="50">
																											<table class="full" align="center" border="0" cellpadding="0" cellspacing="0">
																												<tr>
																													<td class="res-center" style="text-align: right; font-family: 'Raleway', Arial, Sans-serif; font-size: 14px; word-break: break-word; padding: 0 10px;" >
																														x <?php echo $row->qty ?>
																													</td>
																												</tr>
																											</table>
																										</td>
																										<td width="120">
																											<table class="full" align="center" border="0" cellpadding="0" cellspacing="0">
																												<tr>
																													<td class="res-center" style="text-align: right; font-family: 'Raleway', Arial, Sans-serif; font-size: 14px; word-break: break-word; padding: 0 10px;" >
																														<?php echo $this->main->currency($row->qty * $row->price) ?>
																													</td>
																												</tr>
																											</table>
																										</td>
																									</tr>
																								</table>
																							</td>
																						</tr>
																						<tr><td height="8" style="font-size:0px">&nbsp;</td></tr>
                                                                                        <?php } ?>
																					</table>
																				</td>
																			</tr>
																		</table>
																	</td>
																</tr>
																<tr><td height="30" style="font-size:0px" >&nbsp;</td></tr>
																<tr >
																	<td>
																		<table class="full" align="center" border="0" cellpadding="0" cellspacing="0">
																			<tr>
																				<td valign="top">
																					<table width="600" align="left" class="res-full" border="0" cellpadding="0" cellspacing="0">
																						<tr>
																							<td>
																								<table style="width: 100%" align="center" border="0" cellpadding="0" cellspacing="0">
																									<tr>
																										<td width="100">

																											<table class="full" align="center" border="0" cellpadding="0" cellspacing="0">
																												<tr>
																													<td class="res-center" style="text-align: right; font-family: 'Raleway', Arial, Sans-serif; font-size: 14px; word-break: break-word; padding: 0 10px;" >
																														<strong>Total</strong>
																													</td>
																												</tr>
																											</table>
																										</td>
																										<td width="120">
																											<table class="full" align="center" border="0" cellpadding="0" cellspacing="0">
																												<tr>
																													<td class="res-center" style="text-align: right; font-family: 'Raleway', Arial, Sans-serif; font-size: 14px; word-break: break-word; padding: 0 10px;" >
																														<?php echo $this->main->currency($cart_price_total) ?>
																													</td>
																												</tr>
																											</table>
																										</td>
																									</tr>
																								</table>
																							</td>
																						</tr>
																						<tr><td height="8" style="font-size:0px">&nbsp;</td></tr>
																					</table>
																				</td>
																			</tr>
																		</table>
																	</td>
																</tr>
																<!-- column x2 end -->
																<tr><td height="37" style="font-size:0px" >&nbsp;</td></tr>
																<!-- paragraph -->
<!--																<tr >-->
<!--																	<td class="res-center" style="text-align: center; color: #707070; font-family: 'Nunito', Arial, Sans-serif; font-size: 16.5px; letter-spacing: 0.4px; line-height: 23px; word-break: break-word" >-->
<!--																		Lorem ipsum dolor sit amet, consectetur adipiscing elit-->
<!--																		sed do eiusmod tempor incididunt ut labore et-->
<!--																		dolore magna aliqua-->
<!--																	</td>-->
<!--																</tr>-->
																<!-- paragraph end -->
<!--																<tr><td height="70" style="font-size:0px" >&nbsp;</td></tr>-->
															</table>
														</td>
													</tr>
												</table>
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
<!-- ====== Module : Footer ====== -->
<table bgcolor="#F5F5F5" align="center" class="full" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td>
			<table bgcolor="#004DDA" align="center" width="750" class="margin-full" style="background-size: cover; background-position: center; border-radius: 0 0 6px 6px;" border="0" cellpadding="0" cellspacing="0" background="<?php echo base_url() ?>assets/template_front/img/mail/module20-bg01.png">
				<tr>
					<td>
						<table width="600" align="center" class="margin-pad" border="0" cellpadding="0" cellspacing="0">
							<tr><td height="70" style="font-size:0px" >&nbsp;</td></tr>
							<!-- image -->
							<tr >
								<td>
									<table align="center" class="res-full" border="0" cellpadding="0" cellspacing="0">
										<tr>
											<td>
												<table align="center" border="0" cellpadding="0" cellspacing="0">
													<tr>
														<td>
															<img width="80" style="max-width: 80px; width: 100%; display: block; line-height: 0px; font-size: 0px; border: 0px;" src="<?php echo base_url() ?>assets/template_front/img/mail/module20-img01.png">
														</td>
													</tr>
												</table>
											</td>
										</tr>
									</table>
								</td>
							</tr>
							<!-- image end -->
							<tr><td height="20" style="font-size:0px" >&nbsp;</td></tr>
							<tr>
								<td>
									<table align="center" border="0" cellpadding="0" cellspacing="0">
										<!-- image -->
										<tr >
											<td>
												<table align="center" class="res-full" border="0" cellpadding="0" cellspacing="0">
													<tr>
														<td style="padding: 0 4px;">
															<table style="border-radius: 50%; padding: 10px; border: 1px solid white;" align="center" border="0" cellpadding="0" cellspacing="0">
																<tr>
																	<td>
																		<img width="15" style="max-width: 15px; width: 100%; display: block; line-height: 0px; font-size: 0px; border: 0px;" src="<?php echo base_url() ?>assets/template_front/img/mail/module20-img02.png">
																	</td>
																</tr>
															</table>
														</td>
														<td style="padding: 0 4px;">
															<table style="border-radius: 50%; padding: 10px; border: 1px solid white;" align="center" border="0" cellpadding="0" cellspacing="0">
																<tr>
																	<td>
																		<img width="15" style="max-width: 15px; width: 100%; display: block; line-height: 0px; font-size: 0px; border: 0px;" src="<?php echo base_url() ?>assets/template_front/img/mail/module20-img03.png">
																	</td>
																</tr>
															</table>
														</td>
														<td style="padding: 0 4px;">
															<table style="border-radius: 50%; padding: 10px; border: 1px solid white;" align="center" border="0" cellpadding="0" cellspacing="0">
																<tr>
																	<td>
																		<img width="15" style="max-width: 15px; width: 100%; display: block; line-height: 0px; font-size: 0px; border: 0px;" src="<?php echo base_url() ?>assets/template_front/img/mail/module20-img04.png">
																	</td>
																</tr>
															</table>
														</td>
														<td style="padding: 0 4px;">
															<table style="border-radius: 50%; padding: 10px; border: 1px solid white;" align="center" border="0" cellpadding="0" cellspacing="0">
																<tr>
																	<td>
																		<img width="15" style="max-width: 15px; width: 100%; display: block; line-height: 0px; font-size: 0px; border: 0px;" src="<?php echo base_url() ?>assets/template_front/img/mail/module20-img05.png">
																	</td>
																</tr>
															</table>
														</td>
													</tr>
												</table>
											</td>
										</tr>
										<!-- image end -->
									</table>
								</td>
							</tr>
							<tr><td height="30" style="font-size:0px" >&nbsp;</td></tr>
							<!-- dash -->
							<tr >
								<td>
									<table align="center" class="res-full" border="0" cellpadding="0" cellspacing="0">
										<tr>
											<td>
												<table bgcolor="#84b1ff" align="center" style="border-radius: 10px;" border="0" cellpadding="0" cellspacing="0">
													<tr>
														<td width="600" height="1" style="font-size:0px" >&nbsp;</td>
													</tr>
												</table>
											</td>
										</tr>
									</table>
								</td>
							</tr>
							<!-- dash end -->
							<tr><td height="20" style="font-size:0px" >&nbsp;</td></tr>
							<!-- paragraph -->
							<tr >
								<td class="res-center" style="text-align: center; color: white; font-family: 'Nunito', Arial, Sans-serif; font-size: 16.5px; letter-spacing: 0.4px; line-height: 23px; word-break: break-word; opacity: .7;" >
									Venza © All rights reserved &nbsp;<small>•</small>&nbsp;
									<a href="https://example.com" style="color: white; text-decoration: none;">
										Terms &amp; Conditions
									</a>
								</td>
							</tr>
							<!-- paragraph end -->
							<tr><td height="20" style="font-size:0px" >&nbsp;</td></tr>
							<!-- dash -->
							<tr >
								<td>
									<table align="center" class="res-full" border="0" cellpadding="0" cellspacing="0">
										<tr>
											<td>
												<table bgcolor="#84b1ff" align="center" style="border-radius: 10px;" border="0" cellpadding="0" cellspacing="0">
													<tr>
														<td width="600" height="1" style="font-size:0px" >&nbsp;</td>
													</tr>
												</table>
											</td>
										</tr>
									</table>
								</td>
							</tr>
							<!-- dash end -->
							<tr><td height="70" style="font-size:0px" >&nbsp;</td></tr>
						</table>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
<!-- ====== Module : Unsubscribe ====== -->
<table bgcolor="#F5F5F5" align="center" class="full" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td>
			<table bgcolor="#F5F5F5" width="750" align="center" class="margin-full" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td>
						<table width="600" align="center" class="margin-pad" border="0" cellpadding="0" cellspacing="0">
							<tr><td height="50" style="font-size:0px" >&nbsp;</td></tr>
							<tr>
								<td>
									<table align="center" border="0" cellpadding="0" cellspacing="0">
										<tr>
											<td>
												<table class="full" align="center" border="0" cellpadding="0" cellspacing="0">
													<!-- paragraph -->
													<tr >
														<td class="res-center" style="text-align: center; color: #909090; font-family: 'Nunito', Arial, Sans-serif; letter-spacing: 0.4px; line-height: 23px; word-break: break-word; font-size: 15px;" >
															We land on a wrong place?
														</td>
													</tr>
													<!-- paragraph end -->
												</table>
											</td>
											<td>
												<table class="full" align="center" border="0" cellpadding="0" cellspacing="0">
													<!-- paragraph -->
													<tr >
														<td class="res-center" style="text-align: center; color: #304050; font-family: 'Nunito', Arial, Sans-serif; font-size: 16.5px; letter-spacing: 0.4px; line-height: 23px; word-break: break-word; padding-left: 7px; font-weight: 600;" >
															<unsubscribe>Unsubscribe</unsubscribe>
														</td>
													</tr>
													<!-- paragraph end -->
												</table>
											</td>
										</tr>
									</table>
								</td>
							</tr>
							<tr><td height="50" style="font-size:0px" >&nbsp;</td></tr>
						</table>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
</body>
</html>