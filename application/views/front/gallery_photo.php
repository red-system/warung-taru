<section class="page-banner-section">
    <div class="container">
        <h1>Blog</h1>
        <span>What's new from us</span>
    </div>
</section>
<div class="container">
    <ul class="page-ban-list">
        <li><a href="<?= base_url('') ?>">Home</a></li>
        <li><a href="<?= base_url('galeri-foto') ?>">Galeri Foto</a></li>
    </ul>
</div>
<section class="gallery-section">
    <div class="container">
        <div class="title-section">
            <h2>Galeri Foto</h2>
        </div>
        <div class="row">
            <?php foreach($gallery as $row): ?>
                <a class="gallery gallery-zoom col-12 col-md-3 mb-3" href="<?= base_url('upload/images/' . $row->thumbnail) ?>" title="<?= $row->title ?>">
                    <img class="" src="<?= base_url('upload/images/' . $row->thumbnail) ?>" alt="<?= $row->thumbnail_alt ?>">
                </a>
            <?php endforeach; ?>
        </div>
    </div>
</section>