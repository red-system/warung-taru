<main class="bg_gray">
    <div class="top_banner general">
        <div class="opacity-mask d-flex align-items-md-center" data-opacity-mask="rgba(0, 0, 0, 0.1)">
            <div class="container">
                <div class="row justify-content-end">
                    <div class="col-lg-12 col-md-12 text-center">
                        <h1><?php echo $page->title ?></h1>
                        <?php echo $page->title_sub ?>
                    </div>
                </div>
            </div>
        </div>
        <img src="<?php echo $this->main->image_preview_url($page->thumbnail) ?>" class="img-fluid" alt="">
    </div>

    <div class="bg_white">
        <div class="container margin_30">
            <div class="row justify-content-between align-items-center flex-lg-row-reverse content_general_row">
                <div class="col-lg-12">
                    <?php echo $page->description ?>
                </div>
            </div>
        </div>
    </div>
</main>