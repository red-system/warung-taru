<main class="bg_gray">

    <div class="bg_white">
        <div class="container margin_60_35">
            <div class="text-center">
                <i class="ti-alert payment-error-icon"></i>
            </div>
            <h1 class="pb-3 text-center">Terjadi Kesalahan</h1>
            <div class="row">
                <div class="col-lg-12 col-md-12 add_bottom_25 text-center">
                    <p>Mohon Maaf terjadi kesalahan pada proses pemesanan, mohon untuk menghubungi kontak dibawah:</p>
                    <br />
                    Phone/WA : <a href="<?php echo $whatsapp_link ?>"><?php echo $whatsapp ?></a><br />
                    Email : <a href="<?php echo $email_link ?>"><?php echo $email ?></a>
                </div>
            </div>
        </div>
    </div>
</main>