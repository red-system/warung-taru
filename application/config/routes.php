<?php
defined('BASEPATH') or exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/

//$connection = mysqli_connect('localhost', 'root', '', 'gtn_ecommerce');

//$lang_code_list = array('id','jp','en','fr','zh');
$lang_code_list = array();


$route['default_controller'] = 'home';
$route['proweb'] = 'proweb/login';
$route['proweb/login'] = 'proweb/login/process';

$route['404_override'] = 'not_found';
$route['translate_uri_dashes'] = FALSE;
$code = '';
//foreach($lang_code_list as $code) {

/**
 * Produk
 */
$route[$code . 'menu'] = 'product/list_product';
// $route[$code . 'produk'] = 'product/list_product';
// $route[$code . 'produk/row'] = 'product/list_row';
// $route[$code . 'produk/(:num)'] = 'product/list_product';
// $route[$code . 'produk/(:any)'] = 'product/list_category/$1/$2';
// $route[$code . 'produk/(:any)/(:num)'] = 'product/list_category/$1/$2';
// $route[$code . 'produk/(:any)/(:any)'] = 'product/detail/$1/$2';

/**
 * Blog
 */
$route[$code . 'blog'] = 'blog';
$route[$code . 'blog/(:num)'] = 'blog';
$route[$code . 'blog/(:any)'] = 'blog/category/$1';
$route[$code . 'blog/(:any)/(:num)'] = 'blog/category/$1';
$route[$code . 'blog/(:any)/(:any)'] = 'blog/detail/$1/$2';

/**
 * Promo
 */
$route[$code . 'promo'] = 'promo';
$route[$code . 'promo/(:num)'] = 'promo';
$route[$code . 'promo/(:any)'] = 'promo/detail/$1';

/**
 * Cart
 */
// $route[$code . 'cart/add'] = 'cart/add';
// $route[$code . 'cart/list'] = 'cart/list_page';
// $route[$code . 'cart/checkout/process'] = 'cart/checkout_process';
// $route[$code . 'cart/checkout'] = 'cart/checkout_page';
// $route[$code . 'payment/notif'] = 'payment/notif';

/**
 * Wishlist
 */

// $route[$code . 'wishlist'] = 'wishlist';
// $route[$code . 'wishliat/add'] = 'wishlist/add';

// $route[$code . 'cara-belanja'] = 'order';
$route[$code . 'galeri-foto'] = 'gallery_photo';
$route[$code . 'tentang-kami'] = 'about_us';
$route[$code . 'kontak-kami'] = 'contact_us';
$route[$code . 'kontak-kami/kirim'] = 'contact_us/send';
// $route[$code . 'frequently-ask-question'] = 'faq';
// $route[$code . 'kebijakan-dan-privasi'] = 'term';

// $route[$code . 'account'] = 'account';
// $route[$code . 'daftar'] = 'account/signup';
// $route[$code . 'login'] = 'account/login';
// $route[$code . 'login/kirim'] = 'account/login_do';
// $route[$code . 'account/profil'] = 'account/profile';
// $route[$code . 'account/help'] = 'account/help';
// $route[$code . 'account/order-tracking'] = 'account/track_order';
// $route[$code . 'account/order'] = 'account/order_list';
// $route[$code . 'account/address'] = 'account/address';


// $route[$code . 'kategori'] = 'category';
// $route[$code . 'rekomendasi-toko'] = 'recommend';
// $route[$code . 'lokasi'] = 'location';
// $route[$code . 'pengaduan'] = 'report';
// $route[$code . 'toko'] = 'shop_list';

$route[$code . 'testimonial'] = 'testimonial';
//    $route['daftar-harga'] = 'price_list';
// $route[$code . 'frequently-asked-question'] = 'faq';
// $route[$code . 'free-trial'] = 'general/free_trial_send';
// $route[$code . 'free-ebook'] = 'free_ebook';
// $route[$code . 'free-ebook-send'] = 'free_ebook/send';

$route['^' . $code . '/(.+)$'] = "$1";
$route['^' . $code . '$'] = $route['default_controller'];
//}

//$category = mysqli_query($connection, "SELECT id, title FROM category");
//while ($data = mysqli_fetch_array($category)) {
//    $route['produk/' . slug($data['title'])] = "product/list/" . $data['id'];
//    $route['produk/' . slug($data['title']).'/(:num)'] = "product/list/" . $data['id'];
//}

//$blog = mysqli_query($connection, "SELECT id, title FROM blog");
//while ($data = mysqli_fetch_array($blog)) {
//    $route['blog/' . slug($data['title'])] = "blog/detail/" . $data['id'];
//}
//$blog_category = mysqli_query($connection, "SELECT id, title FROM blog_category");
//while ($data = mysqli_fetch_array($blog_category)) {
//    $route['blog/' . slug($data['title'])] = "blog/category/" . $data['id'];
//}

//$product = mysqli_query($connection, "SELECT product.id, product.title, category.title AS category_title FROM product INNER JOIN category ON category.id = product.id_category");
//while ($data = mysqli_fetch_array($product)) {
//    $route['produk/' . slug($data['category_title']).'/'.slug($data['title'])] = "product/detail/" . $data['id'];
//}

function slug($string)
{
    $find = array(' ', '/', '&', '\\', '\'', ',', '(', ')', '!');
    $replace = array('-', '-', 'and', '-', '-', '-', '', '', '');

    $slug = str_replace($find, $replace, strtolower($string));

    return $slug;
}
