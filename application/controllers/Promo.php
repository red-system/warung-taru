<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Promo extends CI_Controller
{
    public function __construct()
    {
        parent:: __construct();
        $this->load->library('main');
    }

    public function index()
    {
        $offset = $this->uri->segment(2);
        $keyword = $_GET['search'];
        $perpage = 6;
        if($offset != null) {
            $offset = ($offset - 1) * $perpage;
        }

        $data = $this->main->data_front();
        $data['keyword'] = $keyword;
        $data['page'] = $this->db->where(array('type' => 'promo', 'id_language' => $data['id_language']))->get('pages')->row();
        $data['page_type'] = 'promo';
        $data['popular_promo'] = $this
            ->db
            ->select('title, code, thumbnail, thumbnail_alt, created_at')
            ->where(array(
                'use' => 'yes',
                'id_language' => $data['id_language'],
            ))
            ->order_by('views', 'DESC')
            ->get('promo', 4, 0)
            ->result();

        if($keyword) {
            $jumlah_data = $this->db
                ->where(array(
                    'id_language' => $data['id_language'],
                    'use'=>'yes'
                ))
                ->like('title', $keyword)
                ->like('description', $keyword)
                ->get('promo')
                ->num_rows();
        } else {
            $jumlah_data = $this->db
                ->where(array(
                    'id_language' => $data['id_language'],
                    'use'=>'yes'
                ))
                ->get('promo')
                ->num_rows();
        }


        $this->load->library('pagination');
        $config['base_url'] = site_url('blog');
        $config['total_rows'] = $jumlah_data;
        $config['per_page'] = $perpage;

        $config['first_link'] = '&laquo;';
        $config['last_link'] = '&raquo;';
        $config['next_link'] = '&gt;';
        $config['prev_link'] = '&lt;';
        $config['full_tag_open'] = '<ul class="pages-list">';
        $config['full_tag_close'] = '</ul>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li><a href="#" class="active">';
        $config['cur_tag_close'] = '</a></li>';
        $config['next_tag_open'] = '<li class="next">';
        $config['next_tagl_close'] = '</li>';
        $config['prev_tag_open'] = '<li class="prev">';
        $config['prev_tagl_close'] = '</li>';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';

        $this->pagination->initialize($config);

        if($keyword) {
            $data['promo_list'] = $this
                ->db
                ->select('title, code, description, thumbnail, thumbnail_alt, created_at, views')
                ->where(array(
                    'id_language' => $data['id_language'],
                    'use'=>'yes'
                ))
                ->like('title', $keyword)
                ->or_like('description', $keyword)
                ->order_by('id', 'DESC')
                ->get('promo', $perpage, $offset)
                ->result();
        } else {
            $data['promo_list'] = $this
                ->db
                ->select('title, code, description, thumbnail, thumbnail_alt, created_at, views')
                ->where(array(
                    'id_language' => $data['id_language'],
                    'use'=>'yes'
                ))
                ->order_by('id', 'DESC')
                ->get('promo', $perpage, $offset)
                ->result();
        }


        $this->template->front('promo', $data);
    }

    public function detail($slug_promo)
    {
        $data = $this->main->data_front();
        $views = $this->db->select('views')->where('slug', $slug_promo)->get('promo')->row()->views;
        $views++;
        $this->db->where('slug', $slug_promo)->update('promo', array('views' => $views));
        $data['views'] = $views;
        $data['page'] = $this
            ->db
            ->select('*')
            ->where('slug', $slug_promo)
            ->order_by('id', 'DESC')
            ->get('promo')
            ->row();
        $data['page']->type = 'promo';

        $data['popular_promo'] = $this
            ->db
            ->select('title, code, thumbnail, thumbnail_alt, created_at')
            ->where(array(
                'use' => 'yes',
                'id_language' => $data['id_language'],
            ))
            ->order_by('views', 'DESC')
            ->get('promo', 4, 0)
            ->result();

        $this->template->front('promo_detail', $data);
    }
}
