<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Product extends CI_Controller
{
    public function __construct()
    {
        parent:: __construct();
        $this->load->library('main');
    }

    public function list_product()
    {
        $data = $this->main->data_front();

//        echo json_encode($data['cart_content']);
//        exit;

        $data['page'] = $this
            ->db
            ->where(array(
                'type' => 'product',
                'id_language' => $data['id_language']
            ))
            ->get('pages')
            ->row();
        
        $data['products'] = $this
            ->db
            ->select('id, title, slug, thumbnail, thumbnail_alt')
            ->where(array('use' => 'yes', 'id_language' => $data['id_language']))
            ->get('category')->result();

        foreach($data['products'] as $row) {
            $row->product = $this->db
                ->select('id, title, title_sub, slug, thumbnail, thumbnail_alt, price')
                ->where(array(
                    'use' => 'yes',
                    'id_language' => $data['id_language'],
                    'id_category' => $row->id,
                ))->order_by('id', 'DESC')
                ->get('product')->result();
        }

        $this->template->front('product_list', $data);
    }

    public function list_category($slug, $step)
    {
        $data = $this->main->data_front();
        $data['page'] = $this
            ->db
            ->where(array(
                'type' => 'category',
                'id_language' => $data['id_language']
            ))
            ->get('pages')
            ->row();
        $data['total_product'] = 0;

        foreach ($data['category_menu'] as $row) {
            $row->product_total = $this
                ->db
                ->where(array(
                    'id_category' => $row->id,
                    'use' => 'yes'
                ))
                ->get('product')
                ->num_rows();

            $data['product_total'] += $row->product_total;
        }

        $perpage = 3;
        $offset = $this->uri->segment(3);
        $base_url_pagination = site_url('produk/'.$slug);
        $id_category = $this->db->select('id')->where('slug', $slug)->get('category')->row()->id;
        $jumlah_data = $this
            ->db
            ->where(array(
                'id_category' => $id_category
            ))
            ->get('product')
            ->num_rows();


        $this->load->library('pagination');
        $config['base_url'] = $base_url_pagination;
        $config['total_rows'] = $jumlah_data;
        $config['per_page'] = $perpage;
        $config['reuse_query_string'] = true;

        $config['first_link'] = 'First';
        $config['last_link'] = 'Last';
        $config['next_link'] = 'Next';
        $config['prev_link'] = 'Prev';
        $config['full_tag_open'] = '<div class="pagging text-center"><nav><ul class="pagination justify-content-center">';
        $config['full_tag_close'] = '</ul></nav></div>';
        $config['num_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['num_tag_close'] = '</span></li>';
        $config['cur_tag_open'] = '<li class="page-item active"><span class="page-link">';
        $config['cur_tag_close'] = '<span class="sr-only">(current)</span></span></li>';
        $config['next_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['next_tagl_close'] = '<span aria-hidden="true">&raquo;</span></span></li>';
        $config['prev_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['prev_tagl_close'] = '</span>Next</li>';
        $config['first_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['first_tagl_close'] = '</span></li>';
        $config['last_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['last_tagl_close'] = '</span></li>';

        $this->pagination->initialize($config);

        $data['product_list'] = $this
            ->db
            ->select('p.*, pg.thumbnail AS product_thumbnail, c.title AS category_title')
            ->join('category c', 'c.id = p.id_category', 'left')
            ->join('product_gallery AS pg', 'pg.id_product = p.id')
            ->where(array(
                'p.id_category' => $id_category,
                'pg.use_thumbnail' => 'yes'
            ))
            ->order_by('category_title', 'ASC')
            ->order_by('p.title', 'ASC')
            ->get('product p', $perpage, $offset)
            ->result();

        $this->template->front('product_list', $data);
    }

    public function list_row()
    {
        $data = $this->main->data_front();
        $data['page'] = $this
            ->db
            ->where(array(
                'type' => 'category',
                'id_language' => $data['id_language']
            ))
            ->get('pages')
            ->row();
        $data['category'] = $this
            ->db
            ->select('title,id')
            ->where(array(
                'use' => 'yes',
                'id_language' => $data['id_language']
            ))
            ->order_by('title', 'ASC')
            ->get('category')
            ->result();

        $jumlah_data = $this
            ->db
            ->where(array(
                'id_language' => $data['id_language']
            ))
            ->get('product')
            ->num_rows();
        $perpage = 8;
        $offset = $this->uri->segment(2);

        $this->load->library('pagination');
        $config['base_url'] = site_url('produk');
        $config['total_rows'] = $jumlah_data;
        $config['per_page'] = $perpage;

        $config['first_link'] = 'First';
        $config['last_link'] = 'Last';
        $config['next_link'] = 'Next';
        $config['prev_link'] = 'Prev';
        $config['full_tag_open'] = '<div class="pagging text-center"><nav><ul class="pagination justify-content-center">';
        $config['full_tag_close'] = '</ul></nav></div>';
        $config['num_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['num_tag_close'] = '</span></li>';
        $config['cur_tag_open'] = '<li class="page-item active"><span class="page-link">';
        $config['cur_tag_close'] = '<span class="sr-only">(current)</span></span></li>';
        $config['next_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['next_tagl_close'] = '<span aria-hidden="true">&raquo;</span></span></li>';
        $config['prev_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['prev_tagl_close'] = '</span>Next</li>';
        $config['first_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['first_tagl_close'] = '</span></li>';
        $config['last_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['last_tagl_close'] = '</span></li>';

        $this->pagination->initialize($config);

        $data['product_list'] = $this
            ->db
            ->select('p.id,p.id_category,p.title,p.thumbnail,p.thumbnail_alt,p.price,p.price_old,p.description,c.title AS category_title')
            ->join('category c', 'c.id = p.id_category', 'left')
            ->where(array(
                'p.id_language' => $data['id_language']
            ))
            ->order_by('category_title', 'ASC')
            ->order_by('p.title', 'ASC')
            ->get('product p', $perpage, $offset)
            ->result();

        $this->template->front('product_list_row', $data);
    }

    public function detail($slug_category, $slug_product)
    {
        $data = $this->main->data_front();
        $bought_limit = 3;
        $data['page'] = $this
            ->db
            ->select('product.*, category.title AS category_title, pg.thumbnail AS product_thumbnail, pg.thumbnail_alt AS product_thumbnail_alt')
            ->join('product_gallery AS pg', 'pg.id_product = product.id')
            ->join('category', 'category.id = product.id_category')
            ->where('product.slug', $slug_product)
            ->where('pg.use_thumbnail', 'yes')
            ->get('product')
            ->row();
        $data['product_gallery'] = $this->db->where(array('id_product' => $data['page']->id, 'use_view' => 'yes'))->get('product_gallery')->result();
        $data['related'] = $this
            ->db
            ->select('p.*, pg.thumbnail AS product_thumbnail, pg.thumbnail_alt AS product_thumbnail_alt, c.title AS category_title')
            ->join('product_gallery AS pg', 'pg.id_product = p.id')
            ->join('category AS c', 'c.id = p.id_category')
            ->where(array(
                'p.id_language' => $data['id_language'],
                'p.use' => 'yes',
                'pg.use_thumbnail' => 'yes'
            ))
            ->order_by('p.title', 'ASC')
            ->get('product p', 6, 0)
            ->result();

        $also_bought = array();
        foreach ($data['related'] as $key => $row) {
            if ($key < $bought_limit) {
                $also_bought[] = $row;
            }
        }

        $data['page']->type = 'category';
        $data['also_bought'] = $this->load->view('front/cart_add_item_bought_list', array('list' => $also_bought), TRUE);
        $data['captcha'] = $this->main->captcha();
        $this->template->front('product_detail', $data);
    }
}
