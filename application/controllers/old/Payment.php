<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Payment extends CI_Controller
{
    public function __construct()
    {
        parent:: __construct();
        $this->load->library('main');
    }

    public function success()
    {
        $data = $this->main->data_front();
        $data['page'] = $this->db->where(array('type' => 'blog', 'id_language' => $data['id_language']))->get('pages')->row();
        $data['page_type'] = 'home';

        $this->template->front('payment_success', $data);
    }

    public function pending()
    {
        $data = $this->main->data_front();
        $data['page'] = $this->db->where(array('type' => 'blog', 'id_language' => $data['id_language']))->get('pages')->row();
        $data['page_type'] = 'home';

        $this->template->front('payment_pending', $data);
    }

    public function error()
    {
        $data = $this->main->data_front();
        $data['page'] = $this->db->where(array('type' => 'blog', 'id_language' => $data['id_language']))->get('pages')->row();
        $data['page_type'] = 'home';

        $this->template->front('payment_error', $data);
    }

    public function notif()
    {
        $post = file_get_contents("php://input");

        $post_2 = json_encode($this->input->post(NULL, TRUE));


        $text = $post ? $post : $post_2;

        $data_midtrans = json_decode($text, TRUE);

        $invoice = $data_midtrans['order_id'];
        $transaction_status = $data_midtrans['transaction_status'];
        $payment_type = $data_midtrans['payment_type'];
        $merchant_id = $data_midtrans['merchant_id'];
        $transaction_time = $data_midtrans['transaction_time'];
        $status = 'pending';

        switch ($payment_type) {
            case "credit_card":
                switch ($transaction_status) {
                    case "capture":
                        $status = $this
                            ->db
                            ->select('status')
                            ->where('invoice', $invoice)
                            ->get('member_cart')
                            ->row()
                            ->status;
                        break;
                    case "deny":
                        $status = 'error';
                        break;
                    case "settlement":
                        $status = 'payment_accepted';
                }
                break;
            case "bank_transfer"
                || "echannel"
                || "bca_klikpay"
                || "bca_klikbca"
                || "mandiri_clickpay"
                || "bri_epay"
                || "cimb_clicks"
                || "danamon_online"
                || "gopay"
                || "cstore"
                || "akulaku":
                switch ($transaction_status) {
                    case "pending":
                        $status = 'pending';
                        break;
                    case "settlement":
                        $status = 'payment_accepted';
                        break;
                    case "expire"
                        || "deny":
                        $status = 'error';
                        break;
                }
                break;
        }

        $data_update = array(
            'status' => $status,
            'payment_type' => $payment_type,
            'merchant_id' => $merchant_id,
            'transaction_time' => $transaction_time
        );

        $this->db->where('invoice', $invoice)->update('member_cart', $data_update);
        $this->db->insert('midtrans', array('invoice' => $invoice, 'text' => $text));

        echo json_encode($text);

    }


}
