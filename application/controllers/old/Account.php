<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Account extends CI_Controller
{
    public function __construct()
    {
        parent:: __construct();
        $this->load->library('main');
    }

    public function index()
    {
        $data = $this->main->data_front();
        $data['page'] = $this->db->where(array('type' => 'contact_us', 'id_language' => $data['id_language']))->get('pages')->row();
        $data['captcha'] = $this->main->captcha();
        $this->template->front('account', $data);
    }

    public function login()
    {
        $login_status = $this->session->userdata('login_status');
        if ($login_status) {
            redirect($this->session->userdata('url_redirect_login'));
        }

        $this->load->library('google');
        $data = $this->main->data_front();
        $data['page'] = $this->db->where(array('type' => 'login', 'id_language' => $data['id_language']))->get('pages')->row();
        $data['captcha'] = $this->main->captcha();

        include_once APPPATH . "vendor/autoload.php";
        $google_client = new Google_Client();
        $google_client->setClientId('407635300401-ftapk4kgm8gphvnkdpv0jrber6r426if.apps.googleusercontent.com'); //Define your ClientID
        $google_client->setClientSecret('h9rJZNh3aL1acepQ5UHHR-Sz'); //Define your Client Secret Key
        $google_client->setRedirectUri('http://dev.redsystem.id/ecommerce/account/signin_google_success'); //Define your Redirect Uri
        $google_client->addScope('email');
        $google_client->addScope('profile');

        $data['login_google'] = $google_client->createAuthUrl();

        $url_redirect_login = $_GET['p'];
        $url_redirect_login = $this->main->str_decrypt($url_redirect_login);
        $this->session->set_userdata(array('url_redirect_login' => $url_redirect_login));

        $this->template->front('account_signin', $data);
    }

    public function login_do()
    {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('email', 'Email', 'trim|required');
        $this->form_validation->set_rules('password', 'Password', 'trim|required|callback_login_check_form');
        $this->form_validation->set_error_delimiters('', '');

        if ($this->form_validation->run() === FALSE) {
            echo json_encode(array(
                'status' => 'error',
                'title' => 'Perhatian',
                'message' => 'Mohon lengkapi Form dengan benar',
                'errors' => array(
                    'email' => form_error('email'),
                    'password' => form_error('password'),
                )
            ));
        } else {
            $member = $this->db->where('email', $this->input->post('email'))->get('member')->row();
            $data = array(
                'login_status' => TRUE,
                'member' => $member
            );
            $this->session->set_userdata($data);

            $this->main->member_login_refill_cart();


            echo json_encode(array(
                'status' => 'success',
            ));
        }
    }

    public function signin_google_success()
    {
        include_once APPPATH . "vendor/autoload.php";
        $google_client = new Google_Client();
        $google_client->setClientId('407635300401-ftapk4kgm8gphvnkdpv0jrber6r426if.apps.googleusercontent.com'); //Define your ClientID
        $google_client->setClientSecret('h9rJZNh3aL1acepQ5UHHR-Sz'); //Define your Client Secret Key
        $google_client->setRedirectUri('http://dev.redsystem.id/ecommerce/account/signin_google_success'); //Define your Redirect Uri
        $google_client->addScope('email');
        $google_client->addScope('profile');

        $token = $google_client->fetchAccessTokenWithAuthCode($_GET["code"]);

        if (!isset($token["error"])) {
            $url_redirect_login = $this->session->userdata('url_redirect_login');
            $google_client->setAccessToken($token['access_token']);
            $this->session->set_userdata('access_token', $token['access_token']);

            $google_service = new Google_Service_Oauth2($google_client);

            $data = $google_service->userinfo->get();

            $current_datetime = date('Y-m-d H:i:s');

            $data_member = array(
                'email' => $data['email'],
                'first_name' => $data['given_name'],
                'last_name' => $data['family_name'],
                'gender' => $data['family_name'],
                'hd' => $data['hd'],
                'id_google' => $data['id'],
                'link' => $data['link'],
                'locale' => $data['locale'],
                'name' => $data['name'],
                'picture' => $data['picture'],
                'created_at' => $current_datetime,
                'last_login' => $current_datetime
            );

            $check = $this->db->where(array('id' => $data['id']))->get('member')->num_rows();
            if ($check == 0) {
                $this->db->insert('member', $data_member);
            } else {
                $this->db->where('id_google', $data['id'])->update('member', array('last_login' => $current_datetime));
            }

            $member = $this->db->where('id_google', $data['id'])->get('member')->row();

            $this->session->set_userdata('member', $member);
            $this->session->set_userdata(array('login_status' => TRUE));

            $this->main->member_login_refill_cart();


            redirect($url_redirect_login);
        } else {
            redirect();
        }

    }

    function signin_facebook_success()
    {
        $this->load->library('facebook');
        if ($this->facebook->is_authenticated()) {
            // Get user info from facebook
            $fbUser = $this->facebook->request('get', '/me?fields=id,first_name,last_name,email,link,gender,picture');

            // Preparing data for database insertion
            $userData['oauth_provider'] = 'facebook';
            $userData['oauth_uid'] = !empty($fbUser['id']) ? $fbUser['id'] : '';;
            $userData['first_name'] = !empty($fbUser['first_name']) ? $fbUser['first_name'] : '';
            $userData['last_name'] = !empty($fbUser['last_name']) ? $fbUser['last_name'] : '';
            $userData['email'] = !empty($fbUser['email']) ? $fbUser['email'] : '';
            $userData['gender'] = !empty($fbUser['gender']) ? $fbUser['gender'] : '';
            $userData['picture'] = !empty($fbUser['picture']['data']['url']) ? $fbUser['picture']['data']['url'] : '';
            $userData['link'] = !empty($fbUser['link']) ? $fbUser['link'] : 'https://www.facebook.com/';

            // Insert or update user data to the database
            $userID = $this->user->checkUser($userData);

            // Check user data insert or update status
            if (!empty($userID)) {
                $data['userData'] = $userData;

                // Store the user profile info into session
                $this->session->set_userdata('userData', $userData);
            } else {
                $data['userData'] = array();
            }

            // Facebook logout URL
            $data['logoutURL'] = $this->facebook->logout_url();
        } else {
            // Facebook authentication url
//            $data['authURL'] = $this->facebook->login_url();

            redirect('login');
        }
    }

    function facebook_logout()
    {
        // Remove local Facebook session
        $this->facebook->destroy_session();
        // Remove user data from session
        $this->session->unset_userdata('userData');
        // Redirect to login page
        redirect('account/facebook_logout');
    }

    function Is_already_register($id)
    {
        return TRUE;
    }

    public function signup()
    {
        $data = $this->main->data_front();
        $data['page'] = $this->db->where(array('type' => 'contact_us', 'id_language' => $data['id_language']))->get('pages')->row();
        $data['captcha'] = $this->main->captcha();
        $this->template->front('account_signup', $data);
    }

    public function profile()
    {
        $this->main->login_check_page();
        $data = $this->main->data_front();
        $member_sess = $this->session->userdata('member');
        $data['page'] = $this->db->where(array('type' => 'member_profile', 'id_language' => $data['id_language']))->get('pages')->row();
        $data['member'] = $this->db->where(array('id' => $member_sess->id))->get('member')->row();

//        echo json_encode($data['page']);

        $member_gender = 'male';

        if ($data['member']->gender) {
            $member_gender = $data['member']->gender;
        }

        $data['member_gender'] = $member_gender;

        $this->template->front('account_profile', $data);
    }

    public function profile_update()
    {
        $this->main->login_check_page();
        $this->load->library('form_validation');
        $this->form_validation->set_rules('first_name', 'Nama Depan', 'trim|required');
        $this->form_validation->set_rules('last_name', 'Nama Belakang', 'trim|required');
        $this->form_validation->set_rules('gender', 'Jenis Kelamin', 'trim|required');
        $this->form_validation->set_rules('birthday', 'Tanggal Lahir', 'trim|required');
        $this->form_validation->set_rules('phone', 'Nomer Telepon', 'trim|required');
        $this->form_validation->set_rules('address', 'Nomer Telepon', 'trim|required');
        $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email|callback_member_email_check');
        $this->form_validation->set_rules('password', 'Password', 'trim');
        if ($this->input->post('password')) {
            $this->form_validation->set_rules('password_conf', 'Konfirmasi Password', 'trim|matches[password]');
        }
        $this->form_validation->set_error_delimiters('', '');

        if ($this->form_validation->run() === FALSE) {
            echo json_encode(array(
                'status' => 'error',
                'title' => 'Perhatian',
                'message' => 'Mohon lengkapi Form dengan benar',
                'errors' => array(
                    'first_name' => form_error('first_name'),
                    'last_name' => form_error('last_name'),
                    'gender' => form_error('gender'),
                    'birthday' => form_error('birthday'),
                    'phone' => form_error('phone'),
                    'address' => form_error('address'),
                    'email' => form_error('email'),
                    'password' => form_error('password'),
                    'password_conf' => form_error('password_conf'),
                )
            ));
        } else {
            $member = $this->session->userdata('member');
            $id = $member->id;

            $first_name = $this->input->post('first_name');
            $last_name = $this->input->post('last_name');
            $gender = $this->input->post('gender');
            $birthday = $this->input->post('birthday');
            $phone = $this->input->post('phone');
            $address = $this->input->post('address');
            $email = $this->input->post('email');
            $password = $this->input->post('password');

            $data_update = array(
                'first_name' => $first_name,
                'last_name' => $last_name,
                'gender' => $gender,
                'birthday' => $birthday,
                'phone' => $phone,
                'address' => $address,
                'email' => $email,
                'updated_at' => date('Y-m-d H:i:s')
            );

            if ($password) {
                $data_update['password'] = md5($password);
            }

            $this->db->where('id', $id)->update('member', $data_update);

            $member_data_new = $this->db->where(array('id' => $id))->get('member')->row();
            $this->session->set_userdata(array('member' => $member_data_new));

            echo json_encode(array(
                'status' => 'success1',
                'title' => 'Berhasil',
                'message' => 'Data Profile berhasil diperbarui'
            ));
        }
    }

    public function help()
    {
        $this->main->login_check_page();
        $data = $this->main->data_front();
        $data['page'] = $this->db->where(array('type' => 'member_help', 'id_language' => $data['id_language']))->get('pages')->row();
        $this->template->front('account_help', $data);
    }

    public function track_order()
    {
        $this->main->login_check_page();
        $data = $this->main->data_front();
        $data['page'] = $this->db->where(array('type' => 'member_track_order', 'id_language' => $data['id_language']))->get('pages')->row();
        $this->template->front('member_track_order', $data);
    }

    public function order_list()
    {
        $this->main->login_check_page();
        $data = $this->main->data_front();
        $data['page'] = $this->db->where(array('type' => 'member_order_list', 'id_language' => $data['id_language']))->get('pages')->row();
        $this->template->front('member_order_list', $data);
    }

    public function address()
    {
        $this->main->login_check_page();
        $data = $this->main->data_front();
        $data['page'] = $this->db->where(array('type' => 'member_address', 'id_language' => $data['id_language']))->get('pages')->row();
        $data['province'] = $this->db->order_by('province_name', 'ASC')->get('province')->result();
        $data['address_list'] = $this
            ->db
            ->join('province', 'province.id_province = member_address.id_province')
            ->join('city', 'city.id_city = member_address.id_city')
            ->join('subdistrict', 'subdistrict.id_subdistrict = member_address.id_subdistrict')
            ->where(array(
                'id_member' => $data['id_member']
            ))
            ->order_by('id', 'ASC')
            ->order_by('primary', 'DESC')
            ->get('member_address')
            ->result();

        $this->template->front('member_address', $data);
    }

    public function address_add()
    {
        $this->main->login_check_page();
        $this->load->library('form_validation');
        $this->form_validation->set_rules('owner_name', 'Nama Pemilik/Toko', 'trim|required');
        $this->form_validation->set_rules('phone', 'Nomer Telepon', 'trim|required');
        $this->form_validation->set_rules('id_province', 'Provinsi', 'trim|required');
        $this->form_validation->set_rules('id_city', 'Kota', 'trim|required');
        $this->form_validation->set_rules('id_subdistrict', 'Kecamatan', 'trim|required');
        $this->form_validation->set_rules('address', 'Alamat Detail', 'trim|required');
        $this->form_validation->set_rules('primary', 'Alamat Utama', 'trim|required');
        $this->form_validation->set_error_delimiters('', '');

        if ($this->form_validation->run() === FALSE) {
            echo json_encode(array(
                'status' => 'error',
                'title' => 'Perhatian',
                'message' => 'Mohon lengkapi Form dengan benar',
                'errors' => array(
                    'owner_name' => form_error('owner_name'),
                    'phone' => form_error('phone'),
                    'id_province' => form_error('id_province'),
                    'id_city' => form_error('id_city'),
                    'id_subdistrict' => form_error('id_subdistrict'),
                    'address' => form_error('address'),
                    'primary' => form_error('primary'),
                )
            ));
        } else {
            $member = $this->session->userdata('member');
            $id_member = $member->id;

            $owner_name = $this->input->post('owner_name');
            $phone = $this->input->post('phone');
            $id_province = $this->input->post('id_province');
            $id_city = $this->input->post('id_city');
            $id_subdistrict = $this->input->post('id_subdistrict');
            $address = $this->input->post('address');
            $primary = $this->input->post('primary');

            if ($primary == 'yes') {
                $this->db->where('id_member', $id_member)->update('member_address', array('primary' => 'no'));
            }

            $data_insert = array(
                'owner_name' => $owner_name,
                'phone' => $phone,
                'id_province' => $id_province,
                'id_city' => $id_city,
                'id_subdistrict' => $id_subdistrict,
                'id_member' => $id_member,
                'address' => $address,
                'primary' => $primary,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            );

            $this->db->insert('member_address', $data_insert);

            echo json_encode(array(
                'status' => 'success',
                'title' => 'Berhasil',
                'message' => 'Alamat baru berhasil disimpan'
            ));
        }
    }

    public function member_address_primary()
    {
        $this->main->login_check_page();
        $member = $this->session->userdata('member');
        $id_member = $member->id;
        $id_member_address = $this->input->post('id_member_address');

        $this->db->where('id_member', $id_member)->update('member_address', array('primary' => 'no'));
        $this->db->where('id', $id_member_address)->update('member_address', array('primary' => 'yes', 'updated_at' => date('Y-m-d H:i:s')));
    }

    public function member_address_delete()
    {
        $this->main->login_check_page();
        $id_member_address = $this->input->post('id_member_address');

        $this->db->where('id', $id_member_address)->delete('member_address');
    }

    public function member_address_edit()
    {
        $this->main->login_check_page();
        $id_member_address = $this->input->post('id_member_address');
        $member_address = $this->db->where('id', $id_member_address)->get('member_address')->row();
        $province = $this
            ->db
            ->select('id_province AS id, province_name AS text')
            ->order_by('province_name', 'ASC')
            ->get('province')
            ->result();
        $city = $this
            ->db
            ->select('id_city AS id, city_name AS text')
            ->where('id_province', $member_address->id_province)
            ->order_by('city_name', 'ASC')
            ->get('city')
            ->result();
        $subdistrict = $this
            ->db
            ->select('id_subdistrict AS id, subdistrict_name AS text')
            ->where('id_city', $member_address->id_city)
            ->order_by('subdistrict_name', 'ASC')
            ->get('subdistrict')
            ->result();

        $data = array(
            'member_address' => $member_address,
            'province' => $province,
            'city' => $city,
            'subdistrict' => $subdistrict
        );

        echo json_encode($data);

    }


    public function address_update()
    {
        $this->main->login_check_page();
        $this->load->library('form_validation');
        $this->form_validation->set_rules('owner_name', 'Nama Pemilik/Toko', 'trim|required');
        $this->form_validation->set_rules('phone', 'Nomer Telepon', 'trim|required');
        $this->form_validation->set_rules('id_province', 'Provinsi', 'trim|required');
        $this->form_validation->set_rules('id_city', 'Kota', 'trim|required');
        $this->form_validation->set_rules('id_subdistrict', 'Kecamatan', 'trim|required');
        $this->form_validation->set_rules('address', 'Alamat Detail', 'trim|required');
        $this->form_validation->set_rules('primary', 'Alamat Utama', 'trim|required');
        $this->form_validation->set_error_delimiters('', '');

        if ($this->form_validation->run() === FALSE) {
            echo json_encode(array(
                'status' => 'error',
                'title' => 'Perhatian',
                'message' => 'Mohon lengkapi Form dengan benar',
                'errors' => array(
                    'owner_name' => form_error('owner_name'),
                    'phone' => form_error('phone'),
                    'id_province' => form_error('id_province'),
                    'id_city' => form_error('id_city'),
                    'id_subdistrict' => form_error('id_subdistrict'),
                    'address' => form_error('address'),
                    'primary' => form_error('primary'),
                )
            ));
        } else {
            $member = $this->session->userdata('member');
            $id_member = $member->id;
            $id = $this->input->post('id');
            $owner_name = $this->input->post('owner_name');
            $phone = $this->input->post('phone');
            $id_province = $this->input->post('id_province');
            $id_city = $this->input->post('id_city');
            $id_subdistrict = $this->input->post('id_subdistrict');
            $address = $this->input->post('address');
            $primary = $this->input->post('primary');

            if ($primary == 'yes') {
                $this->db->where('id_member', $id_member)->update('member_address', array('primary' => 'no'));
            }

            $data_update = array(
                'owner_name' => $owner_name,
                'phone' => $phone,
                'id_province' => $id_province,
                'id_city' => $id_city,
                'id_subdistrict' => $id_subdistrict,
                'id_member' => $id_member,
                'address' => $address,
                'primary' => $primary,
                'updated_at' => date('Y-m-d H:i:s'),
            );

            $this->db->where('id', $id)->update('member_address', $data_update);

            echo json_encode(array(
                'status' => 'success',
                'title' => 'Berhasil',
                'message' => 'Alamat Pengiriman berhasil diperbarui'
            ));
        }
    }

    public function city_get()
    {
        $id_province = $this->input->post('id_province');
        $city = $this
            ->db
            ->select('id_city AS id, city_name AS text')
            ->where(array(
                'id_province' => $id_province
            ))
            ->order_by('city_name', 'ASC')
            ->get('city')
            ->result();

        header('Content-Type: application/json');
        echo json_encode($city);
    }

    public function subdistrict_get()
    {
        $id_city = $this->input->post('id_city');
        $subdistrict = $this
            ->db
            ->select('id_subdistrict AS id, subdistrict_name AS text')
            ->where(array(
                'id_city' => $id_city
            ))
            ->order_by('subdistrict_name', 'ASC')
            ->get('subdistrict')
            ->result();

        header('Content-Type: application/json');
        echo json_encode($subdistrict);
    }

    public function subdistrict_postal_code_get()
    {
        $id_subdistrict = $this->input->post('id_subdistrict');
        $postal_code = $this
            ->db
            ->select('postal_code')
            ->where(array(
                'id_subdistrict' => $id_subdistrict
            ))
            ->get('subdistrict')
            ->row()
            ->postal_code;

        echo $postal_code;
    }

    public function get_province()
    {
        $client = new GuzzleHttp\Client();
        $city = $this->db->get('city')->result();
        $data_insert = array();
        foreach ($city as $row) {
            $res = $client->request('GET', 'https://pro.rajaongkir.com/api/subdistrict?key=2ecc568bbed69df540d3a5941b74b2ca&city=' . $row->city_id, array());
            $response = json_decode($res->getBody(), true);

            $data_insert = array();
            foreach ($response['rajaongkir']['results'] as $row) {
                $data_insert = array(
                    'subdistrict_id' => $row['subdistrict_id'],
                    'city_id' => $row['city_id'],
                    'province_id' => $row['province_id'],
                    'subdistrict_name' => $row['subdistrict_name'],
                );
                //$this->db->insert('subdistrict', $data_insert);
            }
        }


        echo json_encode($data_insert);
    }

    public function logout()
    {
        $this->session->sess_destroy();
        redirect();
    }

    public function login_check_form($str)
    {
        $email = $this->input->post('email');
        $password = md5($this->input->post('password'));

        $check = $this
            ->db
            ->where(array(
                'email' => $email,
                'password' => $password
            ))
            ->get('member')
            ->num_rows();

        if ($check == 0) {
            $this->form_validation->set_message('login_check_form', 'Email atau Password salah');
            return FALSE;
        } else {
            return TRUE;
        }
    }

    public function member_email_check($str)
    {
        $member = $this->session->userdata('member');
        $email = $member->email;
        $check = $this
            ->db
            ->where(array(
                'email' => $str
            ))
            ->where_not_in('email', array($email))
            ->get('member')
            ->num_rows();

        if ($check > 0) {
            $this->form_validation->set_message('member_email_check', 'Email tidak tersedia');
            return FALSE;
        } else {
            return TRUE;
        }
    }
}
