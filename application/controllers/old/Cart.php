<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cart extends CI_Controller
{
    // Production
//    protected $server_key = 'VT-server-tvCQyKYtLG79PjIrUVz4moFv';
//    protected $client_key = 'VT-client-vVPIQY3ZoaWeswpG';
//    protected $midtrans_js_link = 'https://app.midtrans.com/snap/snap.js';
//    protected $midtrans_production = TRUE;

    // Development
    protected $server_key = 'SB-Mid-server-YKLPSe0HVCDHioUCdFVC38Hj';
    protected $client_key = 'SB-Mid-client-CyJEQCYZ_ZIIC9vN';
    protected $midtrans_js_link = 'https://app.sandbox.midtrans.com/snap/snap.js';
    protected $midtrans_production = FALSE;


    public function __construct()
    {
        parent:: __construct();
        $this->load->library('main');
    }

    public function list_page()
    {
        $data = $this->main->data_front();
        $data['page'] = $this->db->where(array('type' => 'cart_list', 'id_language' => $data['id_language']))->get('pages')->row();
        $data['cart_list'] = $this->cart->contents();
        $data['cart_total_item'] = $this->cart->total_items();

        $price_total = 0;
        foreach ($data['cart_list'] as $row) {
            $price_total += $row['subtotal'];
        }

        $data['cart_total_price'] = $price_total;

        $this->template->front('cart_list_page', $data);
    }

    public function checkout_page()
    {
        $this->main->login_check_page();
        $this->main->checkout_product_check();

        $this->main->invoice_number_generate();

        $data = $this->main->data_front();

        $member = $this->session->userdata('member');
        $invoice = $this->db->select('invoice')->where(array('id_member' => $data['id_member'], 'status' => 'start'))->get('member_cart')->row()->invoice;
        $data['page'] = $this->db->where(array('type' => 'cart_checkout', 'id_language' => $data['id_language']))->get('pages')->row();

//        echo $invoice.'--';
//        exit;

        /**
         * Proses Kondisi Cart
         */


        $data['cart_content'] = $this->cart->contents();
        $data['cart_price_total'] = 0;
        foreach ($data['cart_content'] as $row) {
            $data['cart_price_total'] += $row['subtotal'];
        }

        /**
         * Proses Kondisi Courirer
         */

        $origin_address = $this->db->where('id', 1)->get('shop_address_origin')->row();
        $data['member_address'] = $this
            ->db
            ->join('province p', 'p.id_province = ma.id_province')
            ->join('city c', 'c.id_city = ma.id_city')
            ->join('subdistrict s', 's.id_subdistrict = ma.id_subdistrict')
            ->where(array(
                'id_member' => $data['id_member'],
                'primary' => 'yes'
            ))
            ->get('member_address ma')
            ->row();
        $courier_data = $this->db->where(array('use' => 'yes'))->order_by('title', 'ASC')->get('courier')->result();
        $courier_list = '';
        $courier_count = count($courier_data) - 1;

        foreach ($courier_data as $key => $row) {
            $separator = '';
            if ($key != $courier_count) {
                $separator = ':';
            }
            $courier_list .= $row->code . $separator;
        }

        $data_get = 'key=' . $this->main->rajaongkir_apikey() .
            '&origin=' . $origin_address->id_subdistrict .
            '&originType=subdistrict' .
            '&destination=' . $data['member_address']->id_subdistrict .
            '&destinationType=subdistrict' .
            '&weight=1' .
            '&courier=' . $courier_list;

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://pro.rajaongkir.com/api/cost",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => $data_get,
            CURLOPT_HTTPHEADER => array(
                "content-type: application/x-www-form-urlencoded",
                "key: your-api-key"
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        $data['courier'] = json_decode($response, TRUE)['rajaongkir']['results'];

        $data['ship_cost'] = 0;
        foreach ($data['courier'] as $key => $row) {
            foreach ($row['costs'] as $key2 => $row2) {
                if ($key == 0 && $key2 == 0) {
                    $data['ship_cost'] = $row2['cost'][0]['value'];
                    break;
                }
            }
        }

        $data['checkout_total'] = $data['cart_price_total'] + $data['ship_cost'];

        /**
         * Midtrans
         */

        $data['client_key'] = $this->client_key;
        $data['midtrans_js_link'] = $this->midtrans_js_link;
        require_once dirname(__FILE__) . '/../libraries/Midtrans.php';
        \Midtrans\Config::$serverKey = $this->server_key;

        // Uncomment for production environment
        \Midtrans\Config::$isProduction = $this->midtrans_production;

        // Enable sanitization
        \Midtrans\Config::$isSanitized = true;

        // Enable 3D-Secure
        \Midtrans\Config::$is3ds = true;


        $item_details = array();

        foreach ($this->cart->contents() as $row) {
            $item_details[] = array(
                'id' => $row['id'],
                'price' => $row['price'],
                'quantity' => $row['qty'],
                'name' => $row['name']
            );
        }

        $item_details[] = array(
            'id' => rand(),
            'price' => $data['ship_cost'],
            'quantity' => 1,
            'name' => 'Ongkos Kirim'
        );

        $billing_address = array(
            'first_name' => $member->first_name,
            'last_name' => $member->last_name,
            'address' => $member->address,
            'city' => $data['member_address']->city_name,
            'postal_code' => $data['member_address']->postal_code,
            'phone' => $member->phone,
            'country_code' => 'IDN'
        );

        $shipping_address = array(
            'first_name' => $data['member_address']->owner_name,
            'last_name' => "",
            'address' => $data['member_address']->address,
            'city' => $data['member_address']->city_name,
            'postal_code' => $data['member_address']->postal_code,
            'phone' => $data['member_address']->phone,
            'country_code' => 'IDN'
        );

        $customer_details = array(
            'first_name' => $member->first_name,
            'last_name' => $member->last_name,
            'email' => $member->email,
            'phone' => $member->phone,
            'billing_address' => $billing_address,
            'shipping_address' => $shipping_address
        );

        $transaction_details = array(
            'order_id' => $invoice,
            'gross_amount' => $this->cart->total(),
        );
        $transaction = array(
            'transaction_details' => $transaction_details,
            'customer_details' => $customer_details,
            'item_details' => $item_details,
        );


        try {
            $snap_token = \Midtrans\Snap::getSnapToken($transaction);
        } catch (Exception $e) {
            $this->main->invoice_number_generate_when_exist();
            redirect('cart/checkout');
        }

        $data['invoice'] = $invoice;

        $this->template->front('cart_checkout', $data);
    }

    /**
     * Digunakan untuk change snap token saat change courier type and price
     */
    function courier_change_price()
    {
        $ship_cost = $this->input->post('ship_cost');
        $member = $this->session->userdata('member');
        $invoice = $this->db->select('invoice')->where(array('id_member' => $member->id, 'status' => 'start'))->get('member_cart')->row()->invoice;

        $member_address = $this
            ->db
            ->join('province p', 'p.id_province = ma.id_province')
            ->join('city c', 'c.id_city = ma.id_city')
            ->join('subdistrict s', 's.id_subdistrict = ma.id_subdistrict')
            ->where(array(
                'id_member' => $member->id,
                'primary' => 'yes'
            ))
            ->get('member_address ma')
            ->row();

        require_once dirname(__FILE__) . '/../libraries/Midtrans.php';
        \Midtrans\Config::$serverKey = $this->server_key;

        // Uncomment for production environment
        \Midtrans\Config::$isProduction = $this->midtrans_production;

        // Enable sanitization
        \Midtrans\Config::$isSanitized = true;

        // Enable 3D-Secure
        \Midtrans\Config::$is3ds = true;

        $item_details = array();

        foreach ($this->cart->contents() as $row) {
            $item_details[] = array(
                'id' => $row['id'],
                'price' => $row['price'],
                'quantity' => $row['qty'],
                'name' => $row['name']
            );
        }

        $item_details[] = array(
            'id' => rand(),
            'price' => $ship_cost,
            'quantity' => 1,
            'name' => 'Ongkos Kirim'
        );

        $billing_address = array(
            'first_name' => $member->first_name,
            'last_name' => $member->last_name,
            'address' => $member->address,
            'city' => $member_address->city_name,
            'postal_code' => $member_address->postal_code,
            'phone' => $member->phone,
            'country_code' => 'IDN'
        );

        $shipping_address = array(
            'first_name' => $member_address->owner_name,
            'last_name' => "",
            'address' => $member_address->address,
            'city' => $member_address->city_name,
            'postal_code' => $member_address->postal_code,
            'phone' => $member_address->phone,
            'country_code' => 'IDN'
        );

        $customer_details = array(
            'first_name' => $member->first_name,
            'last_name' => $member->last_name,
            'email' => $member->email,
            'phone' => $member->phone,
            'billing_address' => $billing_address,
            'shipping_address' => $shipping_address
        );

        $transaction_details = array(
            'order_id' => $invoice,
            'gross_amount' => $this->cart->total(), // no decimal allowed for creditcard
        );
        $transaction = array(
            'transaction_details' => $transaction_details,
            'customer_details' => $customer_details,
            'item_details' => $item_details,
        );
        $snap_token = \Midtrans\Snap::getSnapToken($transaction);

        $response = array(
            'snap_token' => $snap_token
        );

        echo json_encode($response);
    }

    public function add()
    {
        $member = $this->session->userdata('member');
        $id_member = $member->id;

        $id = $this->input->post('id');
        $title = $this->input->post('title');
        $qty = $this->input->post('qty');
        $price = $this->input->post('price');
        $thumbnail = $this->input->post('thumbnail');
        $category_title = $this->input->post('category_title');

        /**
         * Insert Into Table Cart Temporary
         */

        $check_order_exist = $this->db->where(array('id_member' => $id_member, 'status' => 'start'))->get('member_cart')->num_rows();
        if ($check_order_exist == 0) {
            $data_member_cart_insert = array(
                'id_member' => $id_member,
                'cart_date' => date('Y-m-d'),
                'cart_total' => 0,
                'status' => 'start',
            );

            $this->db->insert('member_cart', $data_member_cart_insert);
            $id_member_cart = $this->db->insert_id();

            $data_insert = array(
                'id_product' => $id,
                'id_member_cart' => $id_member_cart,
                'qty' => $qty,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            );

            $this->db->insert('member_cart_detail', $data_insert);
        } else {

            $id_member_cart = $this
                ->db
                ->select('id')
                ->where(array(
                    'id_member' => $id_member,
                    'status' => 'start'
                ))
                ->get('member_cart')
                ->row()
                ->id;

            $check_product_exist = $this
                ->db
                ->where(array(
                    'id_product' => $id,
                    'id_member_cart' => $id_member_cart
                ))
                ->get('member_cart_detail')
                ->num_rows();

            if ($check_product_exist == 0) {
                $data_insert = array(
                    'id_product' => $id,
                    'id_member_cart' => $id_member_cart,
                    'qty' => $qty,
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s'),
                );

                $this->db->insert('member_cart_detail', $data_insert);
            } else {
                $qty_latest = $this
                    ->db
                    ->select('qty')
                    ->where(array(
                        'id_member_cart' => $id_member_cart,
                        'id_product' => $id
                    ))
                    ->get('member_cart_detail')
                    ->row()
                    ->qty;

                $qty_now = $qty_latest + $qty;

                $data_update = array(
                    'qty' => $qty_now,
                    'updated_at' => date('Y-m-d H:i:s')
                );

                $this->db->where(array('id_member_cart' => $id_member_cart, 'id_product' => $id))->update('member_cart_detail', $data_update);
            }

        }

        /**
         * Insert cart session
         */

        $data = array(
            'id' => $id,
            'qty' => $qty,
            'price' => $price,
            'name' => $title,
            'options' => array(
                'thumbnail' => $thumbnail,
                'category_title' => $category_title,
                'id_member_cart'
            )
        );
        $this->cart->insert($data);

        /**
         * Total Member Cart
         */

        $cart_content = $this->cart->contents();
        $cart_total = 0;
        foreach ($cart_content as $row) {
            $cart_total += $row['subtotal'];
        }

        $this->db->where(array('id_member' => $id_member, 'status' => 'start'))->update('member_cart', array('cart_total' => $cart_total));

        $return = array(
            'cart_list_review' => $this->main->cart_list_review(),
        );

        echo json_encode($return);
    }

    public function remove()
    {
        $member = $this->session->userdata('member');
        $id_member = $member->id;
        $rowid = $this->input->post('rowid');
        $id_product = $this->input->post('id');
        $cart_price_total = 0;
        $cart_content = $this->cart->contents();
        $id_member_cart = $this->db->select('id')->where(array('id_member' => $id_member, 'status' => 'start'))->get('member_cart')->row()->id;

        foreach ($cart_content as $row) {
            $cart_price_total += $row['subtotal'];
        }

        $this->cart->remove($rowid);

        $this->db->where(array('id_product' => $id_product, 'id_member_cart' => $id_member_cart))->delete('member_cart_detail');

        $return = array(
            'cart_list_review' => $this->main->cart_list_review(),
            'cart_item_total' => $this->cart->total_items(),
            'cart_price_total' => $cart_price_total,
            'id_product' => $id_product,
            'rowid' => $rowid
        );

        echo json_encode($return);
    }

    public function destroy()
    {
        $this->cart->destroy();
    }

    public function update()
    {
        $member = $this->session->userdata('member');
        $id_member = $member->id;
        $qty_arr = $this->input->post('qty');
        $id_product = $this->input->post('id_product');
        $data_update = array();
        $id_member_cart = $this->db->select('id')->where(array('id_member' => $id_member, 'status' => 'start'))->get('member_cart')->row()->id;

        foreach ($qty_arr as $rowid => $qty) {
            $data_update[] = array(
                'rowid' => $rowid,
                'qty' => $qty
            );

            $data_update_temp = array(
                'qty' => $qty,
                'updated_at' => date('Y-m-d H:i:s'),
            );

            $this
                ->db
                ->where(array(
                    'id_product' => $id_product[$rowid],
                    'id_member_cart' => $id_member_cart
                ))
                ->update('member_cart_detail', $data_update_temp);

        }

        $this->cart->update($data_update);

        redirect(site_url('cart/list'));
    }

    public function checkout_process()
    {
        $total_items = $this->cart->total_items();
        $login_status = $this->session->userdata('login_status');
        if ($total_items > 0 && $login_status) {
            $data = array(
                'status' => TRUE,
                'type' => NULL,
                'title' => NULL,
                'message' => NULL,
                'button_confirm' => NULL
            );
        } elseif ($total_items == 0) {
            $data = array(
                'status' => FALSE,
                'type' => 'cart_empty',
                'title' => 'Perhatian',
                'message' => 'Keranjang Belanja masih Kosong, silahkan berbelanja terlebih dahulu',
                'button_confirm' => 'Lanjut Belanja'
            );
        } elseif (!$login) {
            $data = array(
                'status' => FALSE,
                'type' => 'login',
                'title' => 'Login / Daftar',
                'message' => 'Mohon Login terlebih dahulu untuk melakukan pembayaran',
                'button_confirm' => 'Login / Daftar'
            );
        } else {
            $data = array(
                'status' => FALSE,
                'type' => NULL,
                'title' => NULL,
                'message' => NULL,
                'button_confirm' => NULL
            );
        }

        echo json_encode($data);
    }

    public function checkout_success()
    {
        $member = $this->session->userdata('member');
        $id_member = $member->id;
        $id_member_cart = $this->db->select('id')->where(array('id_member' => $id_member, 'status' => 'start'))->get('member_cart')->row()->id;

        $this->db->where(array('id' => $id_member_cart))->update('member_cart', array('status' => 'payment_accepted'));


        $this->cart->destroy();
    }

    public function checkout_mail($id_member = 1, $id_member_cart = 15)
    {
        $member_cart = $this->db->where('id', $id_member_cart)->get('member_cart')->row();
        $member_address = $this
            ->db
            ->join('province p', 'p.id_province = ma.id_province')
            ->join('city c', 'c.id_city = ma.id_city')
            ->join('subdistrict s', 's.id_subdistrict = ma.id_subdistrict')
            ->where(array(
                'id_member' => $id_member,
                'primary' => 'yes'
            ))
            ->get('member_address ma')
            ->row();

        $member_cart_detail = $this
            ->db
            ->join('product p', 'p.id = mcd.id_product')
            ->where('mcd.id_member_cart', $id_member_cart)
            ->order_by('mcd.id' ,'ASC')
            ->get('member_cart_detail mcd')
            ->result();

        $data = array(
            'member_cart' => $member_cart,
            'member_address' => $member_address,
            'member_cart_detail' => $member_cart_detail,
            'cart_price_total' => 0
        );

        $body = $this->load->view('front/mail/member_checkout', $data, TRUE);
        $subject = 'Subject';
        $to_email = 'mahendra.adi.wardana@gmail.com';
        $to_name = 'Mahendra Wardana';

        $this->main->mailer_auth($subject, $to_email, $to_name, $body, $file = '');
    }

    public function checkout_pending()
    {
        $member = $this->session->userdata('member');
        $id_member = $member->id;
        $id_member_cart = $this->db->select('id')->where(array('id_member' => $id_member, 'status' => 'start'))->get('member_cart')->row()->id;

        $this->db->where(array('id' => $id_member_cart))->update('member_cart', array('status' => 'pending'));
        $this->cart->destroy();
    }

    public function checkout_error()
    {
        $member = $this->session->userdata('member');
        $id_member = $member->id;
        $id_member_cart = $this->db->select('id')->where(array('id_member' => $id_member, 'status' => 'start'))->get('member_cart')->row()->id;

        $this->db->where(array('id' => $id_member_cart))->update('member_cart', array('status' => 'error'));
        $this->cart->destroy();
    }

    public function view()
    {
        $data = $this->main->data_front();
        $data['page'] = $this->db->where(array('type' => 'contact_us', 'id_language' => $data['id_language']))->get('pages')->row();
        $data['captcha'] = $this->main->captcha();
        $this->template->front('cart_view', $data);
    }

    public function captcha_check($str)
    {
        if ($str == $this->session->userdata('captcha_mwz')) {
            return TRUE;
        } else {
            $this->form_validation->set_message('captcha_check', 'security code was wrong, please fill again truly');
            return FALSE;
        }
    }

    public function send()
    {
        $this->main->login_check_page();

        $this->load->library('form_validation');
        $this->form_validation->set_rules('name', 'Nama', 'required');
        $this->form_validation->set_rules('phone', 'No Telepon', 'required');
        $this->form_validation->set_rules('email', 'Email', 'required|valid_email');
        $this->form_validation->set_rules('message', 'Pesan', 'required');
        $this->form_validation->set_rules('captcha', 'Security Code', 'required|callback_captcha_check');
        $this->form_validation->set_error_delimiters('', '');

        if ($this->form_validation->run() === FALSE) {
            echo json_encode(array(
                'status' => 'error',
                'title' => 'Perhatian',
                'message' => 'Fill form completly',
                'errors' => array(
                    'name' => form_error('name'),
                    'phone' => form_error('phone'),
                    'email' => form_error('email'),
                    'message' => form_error('message'),
                    'captcha' => form_error('captcha'),
                )
            ));
        } else {
            $email_admin = $this->db->where('use', 'yes')->get('email')->result();
            $name = $this->input->post('name');
            $email = $this->input->post('email');
            $phone = $this->input->post('phone');
            $message = $this->input->post('message');


            $message_admin = '

            Dear Admin,<br /><br />
            You have contact us message from web form<br />
            form details as follows:<br /><br />
            
            Name : ' . $name . '<br>
            Email : ' . $email . '<br>
            Telephone : ' . $phone . '<br>
            Message : ' . $message . '<br /><br />
            
            
            Regarding,<br />
            Contact Us System ' . $this->main->web_name() . '<br /><br />' . $this->main->credit();

            $message_user = '
			    Dear ' . $name . ',<br />
			    <br />
			    Thank you for contact us, We will follow up your message as soon as possible ^_^<br />
			    <br /><br />
			    Regarding,<br />
			    ' . $this->main->web_name() . '<br /><Br />' . $this->main->credit();

            $this->main->mailer_auth('Kontak Kami - ' . $this->main->web_name(), $email, $this->main->web_name(), $message_user);

            foreach ($email_admin as $r) {
                $this->main->mailer_auth('Kontak Kami - Website ' . $this->main->web_url(), $r->email, $this->main->web_name() . ' Administrator ', $message_admin);
            }

            echo json_encode(array(
                'status' => 'success',
                'title' => 'Sukses',
                'message' => 'Thank you for contact us, we will follow up you as soon as possible ^_^'
            ));

        }
    }
}
