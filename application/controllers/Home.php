<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Home extends CI_Controller
{
    public function __construct()
    {
        parent:: __construct();
        $this->load->library('main');
    }

    public function index()
    {
        $data = $this->main->data_front();
        
        $data['page'] = $this->db->where(array('type' => 'home', 'id_language' => $data['id_language']))->get('pages')->row();
        
        $data['slider'] = $this->db->where(array('use' => 'yes', 'id_language' => $data['id_language']))->order_by('id', 'ASC')
            ->get('slider')->result();

        foreach($data['slider'] as $row){
            switch ($row->type) {
                case 'menu':
                    $row->button_text = 'View Our Menu';
                    break;
                case 'promo':
                    $row->button_text = 'Get Promo';
                    break;
                case 'blog':
                    $row->button_text = 'View Blog';
                    break;
                default:
                    # code...
                    break;
            }
        }
        
        $data['popular_dishes'] = $this->db->select('id, title, title_sub, price, slug, thumbnail, thumbnail_alt')
            ->where(array('best_seller' => 'yes',))->order_by('updated_at', 'DESC')->limit(6)->get('product')
            ->result();

        $data['service'] = $this->db->select('title, description, thumbnail, thumbnail_alt')->get('service')->result();
        
        $data['best_dish_today'] = $this->db->select('id, title, title_sub, price, slug, thumbnail, thumbnail_alt')
            ->join('product', 'product.id = best_today.product_id')->get('best_today')->result()[0];
        
        $data['galleries'] = $this->db->select('title, thumbnail, thumbnail_alt')->get('gallery', 4, 0)->result();
        
        $data['testimonials'] = $this->db->select('title, description, thumbnail, thumbnail_alt')
            ->where(['use' => 'yes'])->get('comment')->result();

        $data['products'] = $this
            ->db
            ->select('id, title, thumbnail, thumbnail_alt')
            ->where(array('use' => 'yes', 'id_language' => $data['id_language']))
            ->get('category')->result();

        foreach($data['products'] as $row) {
            $row->product = $this->db
                ->select('id, title, title_sub, slug, thumbnail, thumbnail_alt, price')
                ->where(array(
                    'use' => 'yes',
                    'id_language' => $data['id_language'],
                    'id_category' => $row->id,
                ))->order_by('id', 'DESC')->limit(5)
                ->get('product')->result();
        }

        $data['blog_home'] = $this
            ->db
            ->select('blog.title,blog.thumbnail,blog.thumbnail_alt,blog.description,blog.created_at, blog_category.title AS category_title, blog.views, blog.created_at')
            ->join('blog_category', 'blog_category.id = blog.id_blog_category', 'left')
            ->where('blog.use', 'yes')
            ->where('blog.id_language', $data['id_language'])
            ->order_by('blog.id', 'DESC')
            ->get('blog', 3, 0)
            ->result();

        $this->template->front('home', $data);

    }
}
